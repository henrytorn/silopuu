package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Tarvike;
import tietokanta.TarvikeDao;

public class LisaaTarvikeController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField TarvikeNimiTxt;
	
	@FXML
	private TextField TarvikeHintaTxt;
	
	@FXML
	private TextField TarvikeToimenpideaikaTxt;
	
	@FXML
	private TextField TarvikeToimenpidehintaTxt;
	
	@FXML
	public Button tallennaTarvikeBtn;
	
	@FXML
	public Button peruutaBtn;
	
	
	@FXML
	void peruutaTarvikeLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaTarviketIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaTarvike(ActionEvent event) throws SQLException {
		
		// Uusi Tarvike-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Tarvike TarvikeOlio = new Tarvike();
		TarvikeOlio.setId(idGenerator());
		TarvikeOlio.setNimi(TarvikeNimiTxt.getText());
		TarvikeOlio.setKappalehinta(Double.parseDouble(TarvikeHintaTxt.getText()));
		TarvikeOlio.setToimenpideaika(Double.parseDouble(TarvikeToimenpideaikaTxt.getText()));
		TarvikeOlio.setToimenpidehinta(Double.parseDouble(TarvikeToimenpidehintaTxt.getText()));
		
		// TarvikeDao-luokan metodilla lisays tietokantaan
		TarvikeDao p = new TarvikeDao();
		if (p.lisaa(TarvikeOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaTarviketIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
