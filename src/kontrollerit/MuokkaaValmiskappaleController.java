package kontrollerit;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Valmiskappale;
import tietokanta.ValmiskappaleDao;
import kontrollerit.TietokantaController;

public class MuokkaaValmiskappaleController  {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField ValmiskappaleNimiTxt;
	
	@FXML
	private TextField ValmiskappaleKappalehintaTxt;
	
	@FXML
	private TextField ValmiskappaleKokoTxt;
	
	@FXML
	private TextField ValmiskappaleVariTxt;
	
	private Valmiskappale valittuValmiskappale;
	
	@FXML
	void muokkaaValmiskappale(ActionEvent event) throws SQLException {
		Valmiskappale ValmiskappaleOlio = new Valmiskappale();
		
		ValmiskappaleOlio.setNimi(ValmiskappaleNimiTxt.getText());
		ValmiskappaleOlio.setKappalehinta(Double.parseDouble(ValmiskappaleKappalehintaTxt.getText()));
		ValmiskappaleOlio.setKoko(Double.parseDouble(ValmiskappaleKokoTxt.getText()));
		ValmiskappaleOlio.setVari(ValmiskappaleVariTxt.getText());
		ValmiskappaleOlio.setId(getValittuValmiskappale().getId());
		
		ValmiskappaleDao p = new ValmiskappaleDao();
		if (p.paivita(ValmiskappaleOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
	
		// Avataan edellinen ikkuna ja suljetaan nykyinen
		TietokantaController tc = new TietokantaController();
		tc.avaaValmiskappaleIkkuna(event);
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	

	@FXML
	void peruuta(ActionEvent event) throws SQLException {
		TietokantaController tc = new TietokantaController();
		tc.avaaValmiskappaleIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public TextField getValmiskappaleNimiTxt() {
		return ValmiskappaleNimiTxt;
	}

	public void setValmiskappaleNimiTxt(TextField valmiskappaleNimiTxt) {
		ValmiskappaleNimiTxt = valmiskappaleNimiTxt;
	}

	public TextField getValmiskappaleKappalehintaTxt() {
		return ValmiskappaleKappalehintaTxt;
	}

	public void setValmiskappaleKappalehintaTxt(TextField valmiskappaleKappalehintaTxt) {
		ValmiskappaleKappalehintaTxt = valmiskappaleKappalehintaTxt;
	}

	public TextField getValmiskappaleKokoTxt() {
		return ValmiskappaleKokoTxt;
	}

	public void setValmiskappaleKokoTxt(TextField valmiskappaleKokoTxt) {
		ValmiskappaleKokoTxt = valmiskappaleKokoTxt;
	}

	public TextField getValmiskappaleVariTxt() {
		return ValmiskappaleVariTxt;
	}

	public void setValmiskappaleVariTxt(TextField valmiskappaleVariTxt) {
		ValmiskappaleVariTxt = valmiskappaleVariTxt;
	}

	public Valmiskappale getValittuValmiskappale() {
		return valittuValmiskappale;
	}

	public void setValittuValmiskappale(Valmiskappale valittuValmiskappale) {
		this.valittuValmiskappale = valittuValmiskappale;
	}
	
}
