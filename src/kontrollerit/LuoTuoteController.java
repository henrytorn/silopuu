package kontrollerit;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Valmiskappale;
import tietokanta.ValmiskappaleDao;
import mallit.ArkistoKappale;
import mallit.ArkistoTuote;
import mallit.Liitos;
import mallit.Tarvike;
import tietokanta.ArkistoTuoteDao;

public class LuoTuoteController implements Initializable{

	@FXML
	private Button poistaKappaleBtn;
	
	@FXML
	public ListView<String> tuoteEsikatselu;

	@FXML
	public TextField tuotteenNimi;
	
	@FXML
	public static ObservableList<String> listItems = FXCollections.observableArrayList();   

	
	private double kokonaishinta;
	private double tarvikeHinta;
	private double liitosHinta;	
	
	private ArkistoKappale valittuKappale;
	private Liitos valittuLiitos;
	private Tarvike valittuTarvike;
	private LuoTuoteController tama;
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		tuoteEsikatselu.setItems(listItems);
		
	}
	
	@FXML
	void avaaLisaaUusiKappaleIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaUusiKappale.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisää uusi kappale");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    		
    		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
    		currStage.hide();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}

	
	//TOIMII T�LL� HETKELL� VALMIITTEN KAPPALEITTEN KANSSA, FIKSAUS KUNHAN LUO TUOTE OSIO T�YSIN VALMIS
	//OTTAA 
	private void luoArkistoTuote() throws SQLException, IOException {

		double kokonaishinta = 0;
		for (int i=0; i<this.getListItems().size();i++) {
			String row = this.getListItems().get(i);
			String[] strings = {"Kappalehinta: ", "Kokonaishinta: "};
			for (String string : strings) {
				if (row.startsWith(string)) {
					String[] parts = row.split(string);
					try {
						double hinta = Double.parseDouble(parts[1]);
						kokonaishinta += hinta;
					} catch(NumberFormatException e){}
				}	
			}
		}
		ArkistoTuote arkistoTuoteOlio = new ArkistoTuote();
		arkistoTuoteOlio.setNimi(tuotteenNimi.getText());
		arkistoTuoteOlio.setHinta(kokonaishinta);
		

		ArkistoTuoteDao p = new ArkistoTuoteDao();
		if (p.lisaa(arkistoTuoteOlio)) {
			System.out.println("Arkistotuotteen Lisays onnistui");
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	// Poistaa viewlistist� itemit siihen asti kun tulii v�li
	public void poistaKappale(ActionEvent event) {
		int valittuKappale = tuoteEsikatselu.getSelectionModel().getSelectedIndex();
		if (valittuKappale != -1) {


			String poistettavaKappale = null;
			
			do {
				poistettavaKappale = tuoteEsikatselu.getSelectionModel().getSelectedItem();

				final int uusiValittuKappale =
						(valittuKappale == tuoteEsikatselu.getItems().size() - 1)
						? valittuKappale - 1
								: valittuKappale;


				tuoteEsikatselu.getItems().remove(valittuKappale);
				tuoteEsikatselu.getSelectionModel().select(uusiValittuKappale);


				System.out.println(": " + valittuKappale);
				System.out.println("Poistetettu: " + poistettavaKappale);

			}

			while (!poistettavaKappale.isEmpty() && !tuoteEsikatselu.getItems().isEmpty() &&valittuKappale <= tuoteEsikatselu.getItems().size() );
		}
	}


	
	@FXML
	void luoPdf(ActionEvent event) throws SQLException, IOException {
		
		System.out.println("Tar: "+valittuTarvike.getNimi());
		System.out.println("Liit: "+valittuLiitos.getNimi());
		System.out.println("Kpl: "+valittuKappale.getNimi());
//		luoArkistoTuote();
//		
//		  PDFont font = PDType1Font.HELVETICA_BOLD; 
//		  PDFont font2 = PDType1Font.TIMES_ROMAN; 
//		  
//		  String tallennusNimi = "testiPDF";
//	      PDDocument document = new PDDocument();
//	      PDPage page = new PDPage(PDRectangle.A4);   
//	      document.addPage( page );
//	      PDPageContentStream contentStream = new PDPageContentStream(document, page);
//
//	      //LOGON PAIKKA
//	      PDImageXObject logoImage = PDImageXObject.createFromFile("logo.png",document);
//	      contentStream.drawImage(logoImage, 20, 700);
//
//	      //SIVUN KORKEUS JOSTA LASKETAAN MILLOIN UUS SIVU LUODAAN.
//	      PDRectangle mediabox = page.getMediaBox();
//          float margin = 150;
//          float startY = mediabox.getUpperRightY() - margin;
//          float currentY=startY;
//          
//          contentStream.beginText(); 
//	      contentStream.setFont(font, 12);
//	      contentStream.setLeading(16.5f);
//	      contentStream.newLineAtOffset(40, 750);
//
//	      //P�IV�YS 
//	      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy" );  
//	      LocalDateTime now = LocalDateTime.now();  
//	      System.out.println(dtf.format(now));  
//	      
//	      contentStream.newLineAtOffset(400, 0);
//	      contentStream.showText("HINTA-ARVIO" );
//	      contentStream.newLine();
//	      contentStream.showText(dtf.format(now));
//	      contentStream.newLine();
//	      contentStream.showText("TUOTE NIMI" );
//	      
//	      //KOONTI
//	      contentStream.newLineAtOffset(-400, -75);
//	      //contentStream.showText("Tuote nimi");
//	     
//	      //LISTVIEW RIVI RIVILT� L�PI
//	      for(int i=0; i<listItems.size();i++)
//	      {
//	    	  //if (listItems.get(i).toString().trim() != ""){
//	    	  //System.out.println(currentY);
//	    	  currentY -= 16.5f;
//	    	  
//	    	  //JOS SIVU LOPUSSA LUODAAN UUS
//	    	  if(currentY<=margin)
//	    	  { 
//              	  contentStream.endText(); 
//                  contentStream.close();
//                  PDPage new_Page = new PDPage(PDRectangle.A4); 
//                  document.addPage(new_Page);
//                  contentStream = new PDPageContentStream(document, new_Page);
//                  contentStream.beginText();
//                  contentStream.setFont(font, 12);
//                  contentStream.newLineAtOffset(40, 800);
//                  contentStream.setLeading(16.5f);
//                  currentY=startY;
//                 // System.out.println("SIVU VAIHTU!");
//              }
//	    		  contentStream.showText(listItems.get(i));
//	    		  contentStream.newLine();    		    
//	      }  
//	      contentStream.endText();
//	      System.out.println("Tiedot lis�tty");
//	      contentStream.close(); 
//		  document.save(new File(tallennusNimi + ".pdf"));	  
//		  System.out.println("Tiedosto tallennettu");
//		  document.close();

	    }
	
	
	@FXML
	void avaaLisaaValmisKappaleIkkuna(ActionEvent event) throws SQLException, IOException {
		
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LuoTuoteValmisKappale.fxml"));
		loader.load();
		
		LuoTuoteValmiskappaleViewController controller = loader.getController();
		tama = this;
		controller.setTama(tama);
		
		Parent root = loader.getRoot();
    	Stage stage = new Stage();
		stage.setTitle("Lis�� valmis kappale");
		stage.setScene(new Scene(root, 1040, 687));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	
	@FXML
	void avaaLisaaLiitosIkkuna(ActionEvent event) throws SQLException, IOException {
		
		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaTuoteLiitos.fxml"));
		loader.load();
		
		LisaaTuoteLiitosController controller = loader.getController();
		
		// Luodaan tasta ikkunasta uusi objekti, jotta voidaan valittaa tietoja ikkunoiden valilla
		// Esim. tarvike ja liitos objekteja, jolloin tieto ei katoa kun availlaan uusia ikkunoita
		// Taytyy hoitaa joka luokassa erikseen gettereilla ja settereilla edestakaisin.
		tama = this;
		controller.setTama(tama);

		
		Parent root = loader.getRoot();
    	Stage stage = new Stage();
		stage.setTitle("Lisää liitos");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void avaaLisaaTarvikeIkkuna(ActionEvent event) throws SQLException, IOException {
		
		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaTuoteTarvike.fxml"));
		loader.load();
		
		LisaaTuoteTarvikeController controller = loader.getController();
		
		// Luodaan tasta ikkunasta uusi objekti, jotta voidaan valittaa tietoja ikkunoiden valilla
		// Esim. tarvike ja liitos objekteja, jolloin tieto ei katoa kun availlaan uusia ikkunoita
		// Taytyy hoitaa joka luokassa erikseen gettereilla ja settereilla edestakaisin.
		tama = this;
		controller.setTama(tama);

		
		Parent root = loader.getRoot();
    	Stage stage = new Stage();
		stage.setTitle("Lisää tarvike");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}

	
	public ObservableList<String> getListItems() {
		return listItems;
	}

	public ArkistoKappale getValittuKappale() {
		return valittuKappale;
	}

	public void setValittuKappale(ArkistoKappale valittuKappale) {
		this.valittuKappale = valittuKappale;
	}

	public double getKokonaishinta() {
		return kokonaishinta;
	}

	public void setKokonaishinta(double kokonaishinta) {
		this.kokonaishinta = kokonaishinta;
	}

	public double getTarvikeHinta() {
		return tarvikeHinta;
	}

	public void setTarvikeHinta(double tarvikeHinta) {
		this.tarvikeHinta = tarvikeHinta;
	}

	public double getLiitosHinta() {
		return liitosHinta;
	}

	public void setLiitosHinta(double liitosHinta) {
		this.liitosHinta = liitosHinta;
	}

	public Liitos getValittuLiitos() {
		return valittuLiitos;
	}

	public void setValittuLiitos(Liitos valittuLiitos) {
		this.valittuLiitos = valittuLiitos;
	}

	public Tarvike getValittuTarvike() {
		return valittuTarvike;
	}

	public void setValittuTarvike(Tarvike valittuTarvike) {
		this.valittuTarvike = valittuTarvike;
	}
	
}
