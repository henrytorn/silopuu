package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Tarvike;
import mallit.Puutavara;
import tietokanta.DBApurit;
import tietokanta.TarvikeDao;
import kontrollerit.Apumetodeja;

public class TarvikeViewController implements Initializable{

	@FXML private TableView<Tarvike> table;
	@FXML private TableColumn<Tarvike, Integer> id;
	@FXML private TableColumn<Tarvike, String> nimi;
	@FXML private TableColumn<Tarvike, Double> kappalehinta;
	@FXML private TableColumn<Tarvike, Double> toimenpidehinta;
	@FXML private TableColumn<Tarvike, Double> toimenpideaika;
	@FXML
	public Button lisaaTarvikeBtn;
	
	ObservableList<Tarvike> Tarvikelist = FXCollections.observableArrayList();
	
	@FXML
	public TextField hakuTBox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Helat";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Tarvikelist.add(new Tarvike(rs.getInt("id"),rs.getString("nimi"),Apumetodeja.round(rs.getDouble("kappalehinta"),2),Apumetodeja.round(rs.getDouble("toimenpidehinta"),2),
						rs.getDouble("toimenpideaika") ));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<Tarvike, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Tarvike, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("kappalehinta"));
		toimenpidehinta.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("toimenpidehinta"));
		toimenpideaika.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("toimenpideaika"));
		table.setItems(Tarvikelist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeTarvike(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	

	
	@FXML
	void avaaLisaaTarvikeIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaTarvike.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lis�� Tarvike");
    		stage.setScene(new Scene(root, 365, 410));
    		stage.show();
    		
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	

	
	
	
	@FXML
	void avaaMuokkaaTarvikeIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaTarvike.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja olion valinta tableviewista
		MuokkaaTarvikeController controller = loader.getController();
    	Tarvike valittuTarvike = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin tietue taulusta, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuTarvike != null) {
			controller.setValittuTarvike(valittuTarvike);
			controller.getTarvikeHintaTxt().setText(Double.toString(valittuTarvike.getKappalehinta()));
			controller.getTarvikeToimenpideaikaTxt().setText(Double.toString(valittuTarvike.getToimenpideaika()));
			controller.getTarvikeToimenpidehintaTxt().setText(Double.toString(valittuTarvike.getToimenpidehinta()));
			controller.getTarvikeNimiTxt().setText(valittuTarvike.getNimi());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	@FXML
	void poistaTarvike(ActionEvent event) throws SQLException {

    	Tarvike valittuTarvike = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa tarvikkeen " + valittuTarvike.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) { 
    	
    		if (valittuTarvike != null) {
    			try {
    				TarvikeDao dao = new TarvikeDao();
    				if (dao.poista(valittuTarvike.getId())) {
    					System.out.println("Poistettiin Tarvike: " + valittuTarvike.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			Tarvikelist.removeAll(Tarvikelist);
    			initialize(null, null);
    		}
    	}
	}
	
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void haeTarvike(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM helat WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			Tarvikelist.clear();

			while (rs.next()) {
				Tarvikelist.add(new Tarvike(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("toimenpidehinta"), rs.getDouble("toimenpideaika")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
	
}