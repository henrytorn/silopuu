package kontrollerit;

import java.io.IOException;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


// Tama luokka sisaltaa (Aloitussivun) toiminnallisuudet, jotka liitetaan (SceneBuilderin avulla) halutuille käyttöliittyman komponenteille.
// .fxml tiedoston avattua SceneBuilderilla luokka pitaa asettaa vasemmasta laidasta Controller valilehdella kohtaan Controller class, 
// sen jalkeen kayttoliittyman komponenteille voi valita suoraan toiminnallisuudeksi valitun luokan sisaltamia metodeja kohdasta Code.
public class PaaikkunaController {

	@FXML
	void avaaTuoteLisays(ActionEvent event) throws SQLException {
		
		// Uuden ikkunan avaus
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Tuotteen luonti");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaTietokanta(ActionEvent event) throws SQLException {
		
		// Uuden ikkunan avaus
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaHallinta.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Tietokannan hallinta");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaArkisto(ActionEvent event) throws SQLException {
		
		// Uuden ikkunan avaus
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoHallinta.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Arkisto");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaAsetukset(ActionEvent event) throws SQLException {
		
		// Uuden ikkunan avaus
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/Asetukset.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Asetukset");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
}
