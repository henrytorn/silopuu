package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.ArkistoTuote;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet ArkistoKappaleen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class ArkistoTuoteDao implements Dao<ArkistoTuote> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan ArkistoTuote id
	 */
	@Override
	public ArkistoTuote hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM tuotteet WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Arkistotuotetta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return ArkistoTuoteOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki ArkistoKappalet-taulun alkiot 
	 * 
	 * @return	lista kaikista ArkistoKappalet-taulun alkioista
	 */
	@Override
	public List<ArkistoTuote> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM tuotteet";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon ArkistoKappale-oliot tallennetaan
			List<ArkistoTuote> ArkistoTuote = new ArrayList<ArkistoTuote>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi ArkistoKappale-olio apumetodin ArkistoKappaleOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				ArkistoTuote ArkistoTuoteOlio = ArkistoTuoteOlioResultSetista(rs);
				ArkistoTuote.add(ArkistoTuoteOlio);
			}
			
			// Palautetaan lista
			return ArkistoTuote;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun ArkistoKappale-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	ArkistoKappale-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(ArkistoTuote p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
//		String sql = "INSERT INTO tuotteet "
//				+ "(nimi, leveys, korkeus, syvyys, hinta) "
//				+ "VALUES (?, ?, ?, ?, ?)";
		
		String sql = "INSERT INTO tuotteet "
				+ "(nimi, hinta) "
				+ "VALUES (?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getHinta());
			System.out.println(p.getHinta());
//			ps.setDouble(2, p.getLeveys());
//			ps.setDouble(3, p.getKorkeus());
//			ps.setDouble(4, p.getSyvyys());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(ArkistoTuote p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE tuotteet SET "
				+ "nimi=?, hinta=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getHinta());
			ps.setInt(3, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM tuotteet WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * ArkistoKappale-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota ArkistoKappale-luokan olioksi
	 * @return		ArkistoKappale-luokan olio
	 * @throws SQLException
	 */
	public ArkistoTuote ArkistoTuoteOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio ArkistoKappale malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		ArkistoTuote ArkistoTuoteOlio = new ArkistoTuote();
		ArkistoTuoteOlio.setId(rs.getInt("id"));
		ArkistoTuoteOlio.setNimi(rs.getString("nimi"));
		ArkistoTuoteOlio.setHinta(rs.getDouble("hinta"));
		
		
		return ArkistoTuoteOlio;	
	}
	
}
