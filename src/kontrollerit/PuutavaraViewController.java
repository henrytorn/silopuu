package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Puutavara;
import tietokanta.DBApurit;
import tietokanta.PuutavaraDao;
import kontrollerit.Apumetodeja;
public class PuutavaraViewController implements Initializable{

	@FXML 
	private TableView<Puutavara> table;
	
	@FXML 
	private TableColumn<Puutavara, Integer> id;
	
	@FXML 
	private TableColumn<Puutavara, String> nimi;
	
	@FXML 
	private TableColumn<Puutavara, Double> kuutiohinta;
	
	@FXML 
	private TableColumn<Puutavara, Double> neliohinta;
	
	@FXML 
	private TableColumn<Puutavara, Double> tiheys;
	
	@FXML
	public Button lisaaBtn;
	
	@FXML
	public TextField hakuTBox;
	
	ObservableList<Puutavara> Puutavaralist = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Puulajit";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Puutavaralist.add(new Puutavara(rs.getInt("id"),rs.getString("nimi"),Apumetodeja.round(rs.getDouble("kuutiohinta"),2),Apumetodeja.round(rs.getDouble("neliohinta"), 2), rs.getDouble("tiheys")));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<Puutavara, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Puutavara, String>("nimi"));
		kuutiohinta.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("kuutiohinta"));
		neliohinta.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("neliohinta"));
		tiheys.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("tiheys"));
		table.setItems(Puutavaralist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haePuutavara(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	void avaaLisaaIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaPuutavara.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisaa Puutavara");
    		stage.setScene(new Scene(root, 365, 410));
    		stage.show();
    		
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}

	}
	
	@FXML
	void avaaMuokkaaIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaPuutavara.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja Puutavara-olion valinta tableviewista
		MuokkaaPuutavaraController controller = loader.getController();
    	Puutavara valittuPuutavara = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin Puutavara, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuPuutavara != null) {
			controller.setValittuPuutavara(valittuPuutavara);
			controller.getPuutavaraHintaTxt().setText(Double.toString(valittuPuutavara.getKuutiohinta()));
			controller.getPuutavaraNelioHintaTxt().setText(Double.toString(valittuPuutavara.getNeliohinta()));
			controller.getPuutavaraMassaTxt().setText(Double.toString(valittuPuutavara.getTiheys()));
			controller.getPuutavaraNimiTxt().setText(valittuPuutavara.getNimi());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	@FXML
	void poistaPuutavara(ActionEvent event) throws SQLException {
		
    	Puutavara valittuPuutavara = table.getSelectionModel().getSelectedItem();
		// Varoitusikkuna
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Oletko varma");
        alert.setHeaderText("Haluatko varmasti poistaa puutavaran " + valittuPuutavara.getNimi() + "?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){


        	if (valittuPuutavara != null) {
    			try {
    				PuutavaraDao dao = new PuutavaraDao();
    				if (dao.poista(valittuPuutavara.getId())) {
    					System.out.println("Poistettiin Puutavara: " + valittuPuutavara.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
    	
    			Puutavaralist.removeAll(Puutavaralist);
    			initialize(null, null);
        	}
		
        }

	}
	
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	//hakutoiminto puutavaralle
	@FXML
	void haePuutavara(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Puulajit WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			Puutavaralist.clear();

			while (rs.next()) {
				Puutavaralist.add(new Puutavara(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kuutiohinta"),rs.getDouble("neliohinta"),rs.getDouble("tiheys")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}