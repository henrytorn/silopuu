package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Liitos;
import tietokanta.LiitosDao;

public class MuokkaaLiitosController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField liitosNimiTxt;
	
	@FXML
	private TextField liitosHintaTxt;
	
	@FXML
	private TextField liitosTuntihintaTxt;
	
	@FXML
	private TextField liitosAsetteenTekeminenTxt;
	
	@FXML
	private Button tallennaLiitosBtn;
	
	@FXML
	private Button peruutaBtn;
	
	private Liitos valittuLiitos;
	
	@FXML
	void peruutaLiitosMuokkaus (ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLiitosIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void muokkaaLiitos(ActionEvent event) throws SQLException {
		
		Liitos liitosOlio = new Liitos();
		
		// Tassa manuaalisesti laitettu id testitarkoitukseen, silla
		// kayttoliittymassa ei ole mahdollisuutta viela valita haluttua
		// tietuetta eli id:ta
		liitosOlio.setNimi(liitosNimiTxt.getText());
		liitosOlio.setKappalehinta(Double.parseDouble(liitosHintaTxt.getText()));
		liitosOlio.setTuntihinta(Double.parseDouble(liitosTuntihintaTxt.getText()));
		liitosOlio.setAsetteen_tekeminen(Double.parseDouble(liitosAsetteenTekeminenTxt.getText()));
		liitosOlio.setId(valittuLiitos.getId());
		
		LiitosDao p = new LiitosDao();
		if (p.paivita(liitosOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
		
		TietokantaController tc = new TietokantaController();
		tc.avaaLiitosIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public TextField getLiitosNimiTxt() {
		return liitosNimiTxt;
	}

	public void setLiitosNimiTxt(TextField liitosNimiTxt) {
		this.liitosNimiTxt = liitosNimiTxt;
	}

	public TextField getLiitosHintaTxt() {
		return liitosHintaTxt;
	}

	public void setLiitosHintaTxt(TextField liitosHintaTxt) {
		this.liitosHintaTxt = liitosHintaTxt;
	}

	public TextField getLiitosTuntihintaTxt() {
		return liitosTuntihintaTxt;
	}

	public void setLiitosTuntihintaTxt(TextField liitosTuntihintaTxt) {
		this.liitosTuntihintaTxt = liitosTuntihintaTxt;
	}

	public Liitos getValittuLiitos() {
		return valittuLiitos;
	}

	public void setValittuLiitos(Liitos valittuLiitos) {
		this.valittuLiitos = valittuLiitos;
	}
	
}
