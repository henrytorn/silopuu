package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Lisapalvelu;
import tietokanta.LisapalveluDao;

public class MuokkaaLisamaksuController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField lisamaksuNimiTxt;
	
	@FXML
	private TextField lisamaksuHintaTxt;
	
	@FXML
	private TextField lisamaksuAsetteenTekeminenTxt;
	
	@FXML
	private TextField lisamaksuTuntihintaTxt;

	@FXML
	private Button tallennaLisamaksuBtn;
	
	@FXML
	private Button peruutaBtn;
	
	private Lisapalvelu valittuLisamaksu;
	
	@FXML
	void peruutaLisamaksuMuokkaus(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLisamaksutIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}

	
	@FXML
	void muokkaaLisapalvelu(ActionEvent event) throws SQLException {
		
		Lisapalvelu lisapalveluOlio = new Lisapalvelu();
		
		// Tassa manuaalisesti laitettu id testitarkoitukseen, silla
		// kayttoliittymassa ei ole mahdollisuutta viela valita haluttua
		// tietuetta eli id:ta
		lisapalveluOlio.setNimi(lisamaksuNimiTxt.getText());
		lisapalveluOlio.setHinta(Double.parseDouble(lisamaksuHintaTxt.getText()));
		lisapalveluOlio.setId(valittuLisamaksu.getId());
		lisapalveluOlio.setTuntihinta(Double.parseDouble(lisamaksuTuntihintaTxt.getText()));
		lisapalveluOlio.setAsetteen_tekeminen(Double.parseDouble(lisamaksuAsetteenTekeminenTxt.getText()));
		
		LisapalveluDao p = new LisapalveluDao();
		if (p.paivita(lisapalveluOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
		TietokantaController tc = new TietokantaController();
		tc.avaaLisamaksutIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}


	public TextField getLisamaksuNimiTxt() {
		return lisamaksuNimiTxt;
	}


	public void setLisamaksuNimiTxt(TextField lisamaksuNimiTxt) {
		this.lisamaksuNimiTxt = lisamaksuNimiTxt;
	}


	public TextField getLisamaksuHintaTxt() {
		return lisamaksuHintaTxt;
	}


	public void setLisamaksuHintaTxt(TextField lisamaksuHintaTxt) {
		this.lisamaksuHintaTxt = lisamaksuHintaTxt;
	}


	public Lisapalvelu getValittuLisamaksu() {
		return valittuLisamaksu;
	}


	public void setValittuLisamaksu(Lisapalvelu valittuLisamaksu) {
		this.valittuLisamaksu = valittuLisamaksu;
	}


	public TextField getLisamaksuAsetteenTekeminenTxt() {
		return lisamaksuAsetteenTekeminenTxt;
	}


	public void setLisamaksuAsetteenTekeminenTxt(TextField lisamaksuAsetteenTekeminenTxt) {
		this.lisamaksuAsetteenTekeminenTxt = lisamaksuAsetteenTekeminenTxt;
	}


	public TextField getLisamaksuTuntihintaTxt() {
		return lisamaksuTuntihintaTxt;
	}


	public void setLisamaksuTuntihintaTxt(TextField lisamaksuTuntihintaTxt) {
		this.lisamaksuTuntihintaTxt = lisamaksuTuntihintaTxt;
	}
	
}
