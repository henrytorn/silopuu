package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Valmiskappale;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet Valmiskappaleen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class ValmiskappaleDao implements Dao<Valmiskappale> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan Valmiskappalen id
	 */
	@Override
	public Valmiskappale hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Valmiskappale haetaan annetulla parametrilla "id"
		String sql = "SELECT id, nimi, kappalehinta, vari FROM Valmis_kappale WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Valmiskappalea ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return ValmiskappaleOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki Valmiskappalet-taulun alkiot 
	 * 
	 * @return	lista kaikista Valmiskappalet-taulun alkioista
	 */
	@Override
	public List<Valmiskappale> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Valmiskappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Valmis_kappale";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon Valmiskappale-oliot tallennetaan
			List<Valmiskappale> Valmiskappalet = new ArrayList<Valmiskappale>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi Valmiskappale-olio apumetodin ValmiskappaleOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Valmiskappale ValmiskappaleOlio = ValmiskappaleOlioResultSetista(rs);
				Valmiskappalet.add(ValmiskappaleOlio);
			}
			
			// Palautetaan lista
			return Valmiskappalet;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Valmiskappale-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	Valmiskappale-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Valmiskappale p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO Valmis_kappale "
				+ "(nimi, kappalehinta,koko, vari) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getKoko());
			ps.setString(4, p.getVari());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Valmiskappale p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE Valmis_kappale SET "
				+ "nimi=?, kappalehinta=?, koko=? ,vari=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getKoko());
			ps.setString(4, p.getVari());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM Valmis_kappale WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Valmiskappale-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Valmiskappale-luokan olioksi
	 * @return		Valmiskappale-luokan olio
	 * @throws SQLException
	 */
	public Valmiskappale ValmiskappaleOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Valmiskappale malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Valmiskappale ValmiskappaleOlio = new Valmiskappale();
		ValmiskappaleOlio.setId(rs.getInt("id"));
		ValmiskappaleOlio.setNimi(rs.getString("nimi"));
		ValmiskappaleOlio.setKappalehinta(rs.getDouble("kappalehinta"));
		ValmiskappaleOlio.setKoko(rs.getDouble("koko"));
		ValmiskappaleOlio.setVari(rs.getString("vari"));
		
		return ValmiskappaleOlio;	
	}

	
}
