package kontrollerit;

import java.awt.Button;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import mallit.Logo;
import tietokanta.DBApurit;
import tietokanta.LogoDao;

public class AsetuksetController implements Initializable{

	@FXML public TextField kuvapath;
	
	@FXML public Button browseImageBtn;
	

	@FXML public ImageView logoView;
   
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
    
    	DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT logo FROM asetukset";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				
				kuvapath.setText(rs.getString("logo"));
				
				
				File f = new File(kuvapath.getText().toString());
				URI u = f.toURI();
				
				Image image = new Image(u.toString());
		           
	            logoView.setImage(image);
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}

	}
    
    
	@FXML
	void browseImage(ActionEvent event) throws SQLException, IOException {
		
		 FileChooser fileChooser = new FileChooser(); 
		fileChooser.setTitle("Valitse logo");
	    
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Image Files (PNG)", "*.png"));
		
		File file = fileChooser.showOpenDialog(null); 

        if (file != null) { 
              
            kuvapath.setText(file.getAbsolutePath()); 
         
            Image image = new Image(file.toURI().toString());
           
            logoView.setImage(image);
        } 
        muokkaaLogo();
        
		
	}
	
	private void muokkaaLogo() throws SQLException {
		
		Logo LogoOlio = new Logo();
		
		LogoOlio.setNimi(kuvapath.getText());

		
		LogoDao p = new LogoDao();
		if (p.paivita(LogoOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
	
	
	}



}