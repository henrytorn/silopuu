package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Pintakasittelyaine;
import tietokanta.PintakasittelyaineDao;
import kontrollerit.TietokantaController;

public class LisaaPintakasittelyaineController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField pintakasittelyaineNimiTxt;
	
	@FXML
	private TextField pintakasittelyaineHintaTxt;
	
	@FXML
	private TextField pintakasittelyaineRiittoisuusTxt;
	
	@FXML
	private TextField pintakasittelyaineTyomaaraTxt;
	
	@FXML
	public Button tallennaPintakasittelyaineBtn;
	
	@FXML
	public Button peruutaBtn;
	

	@FXML
	void peruutaLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaPintakasittelyaineetIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaPintakasittelyaine(ActionEvent event) throws SQLException {
		
		// Uusi pintakasittelyaine-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Pintakasittelyaine pintakasittelyaineOlio = new Pintakasittelyaine();
		pintakasittelyaineOlio.setId(idGenerator());
		pintakasittelyaineOlio.setNimi(pintakasittelyaineNimiTxt.getText());
		pintakasittelyaineOlio.setLitrahinta(Double.parseDouble(pintakasittelyaineHintaTxt.getText()));
		pintakasittelyaineOlio.setRiittoisuus(Double.parseDouble(pintakasittelyaineRiittoisuusTxt.getText()));
		
		// PintakasittelyaineDao-luokan metodilla lisays tietokantaan
		PintakasittelyaineDao p = new PintakasittelyaineDao();
		if (p.lisaa(pintakasittelyaineOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaPintakasittelyaineetIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	@FXML
	void muokkaaPintakasittelyaine(ActionEvent event) throws SQLException {
		
		Pintakasittelyaine pintakasittelyaineOlio = new Pintakasittelyaine();
		
		// Tassa manuaalisesti laitettu id testitarkoitukseen, silla
		// kayttoliittymassa ei ole mahdollisuutta viela valita haluttua
		// tietuetta eli id:ta
		pintakasittelyaineOlio.setNimi(pintakasittelyaineNimiTxt.getText());
		pintakasittelyaineOlio.setLitrahinta(Double.parseDouble(pintakasittelyaineHintaTxt.getText()));
		pintakasittelyaineOlio.setRiittoisuus(Double.parseDouble(pintakasittelyaineRiittoisuusTxt.getText()));
		pintakasittelyaineOlio.setId(282);
		
		PintakasittelyaineDao p = new PintakasittelyaineDao();
		if (p.paivita(pintakasittelyaineOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
