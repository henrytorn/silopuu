package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Pintakasittelyaine;
import tietokanta.DBApurit;
import tietokanta.PintakasittelyaineDao;
import kontrollerit.Apumetodeja;

public class PintakasittelyaineViewController implements Initializable{

	@FXML private TableView<Pintakasittelyaine> table;
	@FXML private TableColumn<Pintakasittelyaine, Integer> id;
	@FXML private TableColumn<Pintakasittelyaine, String> nimi;
	@FXML private TableColumn<Pintakasittelyaine, Double> litrahinta;
	@FXML private TableColumn<Pintakasittelyaine, Double> riittoisuus;
	@FXML public Button lisaaPintakasittelyaineBtn;
	ObservableList<Pintakasittelyaine> pkasittelyainelist = FXCollections.observableArrayList();
	@FXML
	public TextField hakuTBox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM pintakasittelyaineet";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				pkasittelyainelist.add(new Pintakasittelyaine(rs.getInt("id"),rs.getString("nimi"),Apumetodeja.round(rs.getDouble("litrahinta"), 2),rs.getDouble("riittoisuus") ));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, String>("nimi"));
		litrahinta.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, Double>("litrahinta"));
		riittoisuus.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, Double>("riittoisuus"));
		table.setItems(pkasittelyainelist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haePkasittelyaine(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	

	
	
	
	
	@FXML
	void avaaLisaaPintakasittelyaineIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaPintakasittelyaine.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisaa pintakasittelyaine");
    		stage.setScene(new Scene(root, 365, 410));
    		stage.show();
    		
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}

	@FXML
	void avaaMuokkaaPintakasittelyaineIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaPintakasittelyaine.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja olion valinta tableviewista
		MuokkaaPintakasittelyaineController controller = loader.getController();
    	Pintakasittelyaine valittuPintakasittelyaine = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin tietue taulusta, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuPintakasittelyaine != null) {
			controller.setValittuPintakasittelyaine(valittuPintakasittelyaine);
			controller.getPintakasittelyaineHintaTxt().setText(Double.toString(valittuPintakasittelyaine.getLitrahinta()));
			controller.getPintakasittelyaineRiittoisuusTxt().setText(Double.toString(valittuPintakasittelyaine.getRiittoisuus()));
			controller.getPintakasittelyaineNimiTxt().setText(valittuPintakasittelyaine.getNimi());
			System.out.println(valittuPintakasittelyaine.toString());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	
	
	
	@FXML
	void poistaPintakasittelyaine(ActionEvent event) throws SQLException {

    	Pintakasittelyaine valittuPintakasittelyaine = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa pintakäsittelyaineen " + valittuPintakasittelyaine.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) { 
    	
    		if (valittuPintakasittelyaine != null) {
    			try {
    				PintakasittelyaineDao dao = new PintakasittelyaineDao();
    				if (dao.poista(valittuPintakasittelyaine.getId())) {
    					System.out.println("Poistettiin pintakasittelyaine: " + valittuPintakasittelyaine.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			pkasittelyainelist.removeAll(pkasittelyainelist);
    			initialize(null, null);
    		}
    	}
	}
	
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void haePkasittelyaine(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Pintakasittelyaineet WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			pkasittelyainelist.clear();

			while (rs.next()) {
				pkasittelyainelist.add(new Pintakasittelyaine(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("litrahinta"),rs.getDouble("riittoisuus")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}