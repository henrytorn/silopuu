package tietokanta;

import java.sql.SQLException;
import java.util.List;


// Abstrakteja metodeja sisaltava interface CRUD -toimintojen implementoinnille.
public interface Dao<T> {

	T hae(int id) throws SQLException;
	List<T> haeKaikki() throws SQLException;
	boolean lisaa(T t) throws SQLException;
	boolean paivita(T t) throws SQLException;
	boolean poista(int p) throws SQLException;
	
}
