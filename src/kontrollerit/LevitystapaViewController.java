package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Levitystapa;
import tietokanta.DBApurit;
import tietokanta.LevitystapaDao;
import kontrollerit.Apumetodeja;

public class LevitystapaViewController implements Initializable{

	@FXML private TableView<Levitystapa> table;
	@FXML private TableColumn<Levitystapa, Integer> idClmn;
	@FXML private TableColumn<Levitystapa, String> nimiClmn;
	@FXML private TableColumn<Levitystapa, Double> perusmaksuClmn;
	@FXML private TableColumn<Levitystapa, Double> tuntihintaClmn;
	@FXML private TableColumn<Levitystapa, Double> aikaClmn;
	
	@FXML public Button lisaaLevitystapaBtn;
	
	@FXML public TextField hakuTBox;
	
	ObservableList<Levitystapa> Levitystapalist = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Levitystapa";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Levitystapalist.add(new Levitystapa(rs.getInt("id"),rs.getString("nimi"),Apumetodeja.round(rs.getDouble("perusmaksu"), 2),rs.getDouble("aika"), 
						Apumetodeja.round(rs.getDouble("tuntihinta"), 2)));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		idClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Integer>("id"));
		nimiClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, String>("nimi"));
		perusmaksuClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("perusmaksu"));
		aikaClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("aika"));
		tuntihintaClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("tuntihinta"));
		table.setItems(Levitystapalist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeLevitystapa(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	void avaaLisaaIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaLevitystapa.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisaa levitystapa");
    		stage.setScene(new Scene(root, 365, 407));
    		stage.show();
    		
    		Stage stages = (Stage) lisaaLevitystapaBtn.getScene().getWindow();
    		stages.close();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}

	}
	
	@FXML
	void avaaMuokkaaIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaLevitystapa.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja Puutavara-olion valinta tableviewista
		MuokkaaLevitystapaController controller = loader.getController();
    	Levitystapa valittuLevitystapa = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin Puutavara, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
	if (valittuLevitystapa != null) {
			controller.setValittuLevitystapa(valittuLevitystapa);
			controller.getLevitystapaPerusmaksuTxt().setText(Double.toString(valittuLevitystapa.getPerusmaksu()));
			controller.getLevitystapaAikaTxt().setText(Double.toString(valittuLevitystapa.getAika()));
			controller.getLevitystapaTuntihintaTxt().setText(Double.toString(valittuLevitystapa.getTuntihinta()));
			controller.getLevitystapaNimiTxt().setText(valittuLevitystapa.getNimi());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 407));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	@FXML
	void poistaLevitystapa(ActionEvent event) throws SQLException {

    	Levitystapa valittuLevitystapa = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa levitystavan " + valittuLevitystapa.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) {
    	
    	
    		if (valittuLevitystapa != null) {
    			try {
    				LevitystapaDao dao = new LevitystapaDao();
    				if (dao.poista(valittuLevitystapa.getId())) {
    					System.out.println("Poistettiin Levitystapa: " + valittuLevitystapa.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			Levitystapalist.removeAll(Levitystapalist);
    			initialize(null, null);
    		}
    	}
	}
	
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
		
	@FXML
	void haeLevitystapa(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Levitystapa WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			Levitystapalist.clear();

			while (rs.next()) {
				Levitystapalist.add(new Levitystapa(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("perusmaksu"),rs.getDouble("aika"),rs.getDouble("tuntihinta")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}