package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.ArkistoKappale;
import kontrollerit.Apumetodeja;
/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet ArkistoKappaleen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class ArkistoKappaleDao implements Dao<ArkistoKappale> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan ArkistoKappalen id
	 */
	@Override
	public ArkistoKappale hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM arkisto_kappaleet WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return ArkistoKappaleOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki ArkistoKappalet-taulun alkiot 
	 * 
	 * @return	lista kaikista ArkistoKappalet-taulun alkioista
	 */
	@Override
	public List<ArkistoKappale> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM arkisto_kappaleet";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon ArkistoKappale-oliot tallennetaan
			List<ArkistoKappale> ArkistoKappalet = new ArrayList<ArkistoKappale>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi ArkistoKappale-olio apumetodin ArkistoKappaleOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				ArkistoKappale ArkistoKappaleOlio = ArkistoKappaleOlioResultSetista(rs);
				ArkistoKappalet.add(ArkistoKappaleOlio);
			}
			
			// Palautetaan lista
			return ArkistoKappalet;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun ArkistoKappale-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	ArkistoKappale-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(ArkistoKappale p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO arkisto_kappaleet "
				+ "(nimi, leveys, korkeus, syvyys, hinta, pintakasittely_id, puutavara_id, levitystapa_id, lisamaksu_id, vari, koko) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLeveys());
			ps.setDouble(3, p.getKorkeus());
			ps.setDouble(4, p.getSyvyys());
			ps.setDouble(5, p.getHinta());
			ps.setInt(6, p.getPintakasittely_id());
			ps.setInt(7, p.getPuutavara_id());
			ps.setInt(8, p.getLevitystapa_id());
			ps.setInt(9, p.getLisamaksu_id());
			ps.setString(10, p.getVari());
			ps.setString(11, p.getKoko());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(ArkistoKappale p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE arkisto_kappaleet SET "
				+ "nimi=?, leveys=?, korkeus=?, syvyys=?, hinta=?, pintakasittely_id=?, puutavara_id=?, levitystapa_id=?, lisamaksu_id=, vari=?, koko=??"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLeveys());
			ps.setDouble(3, p.getKorkeus());
			ps.setDouble(4, p.getSyvyys());
			ps.setDouble(5, p.getHinta());
			ps.setInt(6, p.getPintakasittely_id());
			ps.setInt(7, p.getPuutavara_id());
			ps.setInt(8, p.getLevitystapa_id());
			ps.setInt(9, p.getLisamaksu_id());
			ps.setString(10, p.getVari());
			ps.setString(11, p.getKoko());
			ps.setInt(12, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM arkisto_kappaleet WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * ArkistoKappale-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota ArkistoKappale-luokan olioksi
	 * @return		ArkistoKappale-luokan olio
	 * @throws SQLException
	 */
	public ArkistoKappale ArkistoKappaleOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio ArkistoKappale malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		ArkistoKappale ArkistoKappaleOlio = new ArkistoKappale();
		ArkistoKappaleOlio.setId(rs.getInt("id"));
		ArkistoKappaleOlio.setNimi(rs.getString("nimi"));
		ArkistoKappaleOlio.setHinta(Apumetodeja.round(rs.getDouble("hinta"), 2));
		ArkistoKappaleOlio.setLeveys(rs.getDouble("leveys"));
		ArkistoKappaleOlio.setKorkeus(rs.getDouble("korkeus"));
		ArkistoKappaleOlio.setSyvyys(rs.getDouble("syvyys"));
		ArkistoKappaleOlio.setPintakasittely_id(rs.getInt("pintakasittely_id"));
		ArkistoKappaleOlio.setPuutavara_id(rs.getInt("puutavara_id"));
		ArkistoKappaleOlio.setLevitystapa_id(rs.getInt("levitystapa_id"));
		ArkistoKappaleOlio.setLisamaksu_id(rs.getInt("lisamaksu_id"));
		ArkistoKappaleOlio.setVari(rs.getString("vari"));
		ArkistoKappaleOlio.setKoko(rs.getString("koko"));
		
		return ArkistoKappaleOlio;	
	}
	
}
