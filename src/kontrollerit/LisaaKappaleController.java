package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import mallit.ArkistoKappale;
import mallit.Levitystapa;
import mallit.Lisapalvelu;
import mallit.Pintakasittelyaine;
import mallit.Puutavara;
import mallit.Valmiskappale;
import tietokanta.ArkistoKappaleDao;
import tietokanta.DBApurit;
import tietokanta.LisapalveluDao;
import tietokanta.PuutavaraDao;
import tietokanta.ValmiskappaleDao;


// TODO !! KAPPALEELLE EI VOIDA VIELA LISATA KUNNOLLA LISAMAKSUJA, LIITOSTEN SEKA TARVIKKEIDEN LIITTAMINEN KAPPALEISIIN MYOS KESKEN
public class LisaaKappaleController implements Initializable{
	
	
	@FXML 
	private Button peruutaKappaleenlisaysBtn;
	
	@FXML 
	private Button lisaaPintakasittelyBtn;
	
	@FXML 
	private TableView<Puutavara> table;
	
	@FXML 
	private TableColumn<Puutavara, Integer> id;
	
	@FXML 
	private TableColumn<Puutavara, String> nimi;
	
	@FXML 
	private TableColumn<Puutavara, Double> kuutiohinta;
	
	@FXML 
	private TableColumn<Puutavara, Double> neliohinta;
	
	@FXML 
	private TableColumn<Puutavara, Double> tiheys;

	@FXML 
	private TextField kokonaisHinta;
	
	@FXML 
	private TextField leveysTextBox;
	
	@FXML 
	private TextField korkeusTextBox;
	
	@FXML 
	private TextField syvyysTextBox;
	
	@FXML
	private TextField pkasittelyTxt, levitystapaTxt;
	
	@FXML
	private TextField kappaleNimiTxt;
	
	@FXML
	private CheckBox liimalevyCheckBox;
	
	@FXML
	private TextField variTxt;
	
	@FXML
	private TextField kokoTxt;
	
	@FXML
	private CheckBox hiontaCheckBox;
	
	@FXML
	private ComboBox<Lisapalvelu> lisamaksuComboBox;
	
	@FXML
	private Button poistaLisamaksu;
	
	private Lisapalvelu hiontaOlio;
	private LisapalveluDao lisapalveluDao;
	private List<Lisapalvelu> lisapalveluLista;
	private ObservableList<Lisapalvelu> lisapalveluObsLista = FXCollections.observableArrayList();
	private double pintakasittelyKokonaishinta;
	private Pintakasittelyaine pkasittelyaineOlio;
	private Levitystapa levitystapaOlio;
	private Puutavara valittuPuutavara;
	private PuutavaraDao puutavaraDao;
	private List<Puutavara> puutavaraLista;
	private ObservableList<Puutavara> puutavaraObsLista = FXCollections.observableArrayList();
	private double valittu_leveys, valittu_korkeus, valittu_syvyys, valittu_hinta;
	private int valittu_id;
	private Lisapalvelu valittuLisapalvelu;
	private ArkistoKappale uusiKappale;
	private ArkistoKappaleDao akp;
	private LisaaKappaleController tama;
	private boolean hiotaanko;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// Puutavara listan alustus ja taytatminen
		puutavaraDao = new PuutavaraDao();
		puutavaraLista = new ArrayList<Puutavara>();
		
		try {
			puutavaraLista = puutavaraDao.haeKaikki();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (Puutavara x : puutavaraLista) {
			puutavaraObsLista.add(x);
		}
		
		id.setCellValueFactory(new PropertyValueFactory<Puutavara, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Puutavara, String>("nimi"));
		kuutiohinta.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("kuutiohinta"));
		neliohinta.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("neliohinta"));
		tiheys.setCellValueFactory(new PropertyValueFactory<Puutavara, Double>("tiheys"));
		
		table.setItems(puutavaraObsLista);
		
		
		// Lisamaksut dropdownin tayttaminen
		
		lisapalveluDao = new LisapalveluDao();
		
		lisapalveluLista = new ArrayList<Lisapalvelu>();
		
		try {
			lisapalveluLista = lisapalveluDao.haeKaikki();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (Lisapalvelu x : lisapalveluLista) {
			lisapalveluObsLista.add(x);
		}
		
		lisamaksuComboBox.setItems(lisapalveluObsLista);
		
		// Naytetaan vain nimet dropdownissa
		lisamaksuComboBox.setConverter(new StringConverter<Lisapalvelu>(){

			@Override
			public Lisapalvelu fromString(String arg0) {
				return null;
			}

			@Override
			public String toString(Lisapalvelu arg0) {
				return arg0.getNimi();
			}
			
		});
		
		//texfieldien listeneriit automaattimeen paivitykseen
		leveysTextBox.textProperty().addListener((observable, oldValue, newValue) -> {
			
			if(valittuPuutavara.getKuutiohinta() != 0) {
	    		try {
					laskeHintaTilavuus();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	if(valittuPuutavara.getNeliohinta() != 0) {
	    		try {
					laskeHintaPintaAla();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
		});
		
		korkeusTextBox.textProperty().addListener((observable, oldValue, newValue) -> {
			if(valittuPuutavara.getKuutiohinta() != 0) {
	    		try {
					laskeHintaTilavuus();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	if(valittuPuutavara.getNeliohinta() != 0) {
	    		try {
					laskeHintaPintaAla();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
		});
		
		syvyysTextBox.textProperty().addListener((observable, oldValue, newValue) -> {
			if(valittuPuutavara.getKuutiohinta() != 0) {
	    		try {
					laskeHintaTilavuus();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
		});
		
		hiontaCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				if(valittuPuutavara.getKuutiohinta() != 0) {
		    		try {
						laskeHintaTilavuus();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
		    	if(valittuPuutavara.getNeliohinta() != 0) {
		    		try {
						laskeHintaPintaAla();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
			}
		});
		
		// ComboBox listener
		lisamaksuComboBox.getSelectionModel().selectedItemProperty().addListener((arg0, oldValue, newValue) -> {
				valittuLisapalvelu = lisamaksuComboBox.getSelectionModel().getSelectedItem();
				
		    	if(valittuPuutavara.getNeliohinta() != 0) {
		    		syvyysTextBox.setDisable(true);
		    		syvyysTextBox.setText("0");
		    		try {
						laskeHintaPintaAla();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		    	}
		    	if(valittuPuutavara.getKuutiohinta() != 0) {
		    		syvyysTextBox.setDisable(false);
		    		try {
						laskeHintaTilavuus();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	} 
		});
		
		
		//tablen listener p�ivitt�miseen
		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    if (newSelection != null) {
		    	setValittuPuutavara(newSelection);
		    	if(valittuPuutavara.getNeliohinta() != 0) {
		    		syvyysTextBox.setDisable(true);
		    		syvyysTextBox.setText("0");
		    		try {
						laskeHintaPintaAla();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		    	}
		    	if(valittuPuutavara.getKuutiohinta() != 0) {
		    		syvyysTextBox.setDisable(false);
		    		try {
						laskeHintaTilavuus();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	} 
		    }});
		}
			
		
	private void laskeHintaTilavuus() throws SQLException {

		valittuPuutavara = table.getSelectionModel().getSelectedItem();
		if (valittuPuutavara != null) {	
		
			double leveys = Double.parseDouble(leveysTextBox.getText());
			double korkeus = Double.parseDouble(korkeusTextBox.getText());
			double syvyys = Double.parseDouble(syvyysTextBox.getText());
			
			double pHinta = leveys / 1000 * korkeus / 1000 * syvyys / 1000 * (valittuPuutavara.getKuutiohinta());
			
			double summa = Math.round((pHinta + pintakasittelyKokonaishinta) * 100.0) / 100.0;
			
			// hionta?
			if (hiontaCheckBox.isSelected()) {
				lisapalveluDao = new LisapalveluDao();
				hiontaOlio = lisapalveluDao.hae(846);
				summa = summa + hiontaOlio.getHinta();
			}
			else {
			}

			// Lisapalvelun hinta mukaan, lasketaan joko kertahinta tai tuntihinta
			if (valittuLisapalvelu != null) {
				if(valittuLisapalvelu.getTuntihinta() != 0) {
					double hinta = valittuLisapalvelu.getTuntihinta() * (valittuLisapalvelu.getAsetteen_tekeminen()/60);
					summa = summa +hinta;
				}
				else if (valittuLisapalvelu.getTuntihinta() == 0) {
					summa = summa + valittuLisapalvelu.getHinta();
				}
			}
			
			kokonaisHinta.setText(Double.toString(summa));
	
		}
	}

	private void laskeHintaPintaAla() throws SQLException {
		
		Puutavara valittuPuutavara = table.getSelectionModel().getSelectedItem();
		if (valittuPuutavara != null) {
		
		double leveys = Double.parseDouble(leveysTextBox.getText());
		double korkeus = Double.parseDouble(korkeusTextBox.getText());
		
		double pHinta = leveys / 1000 * korkeus / 1000 * (valittuPuutavara.getNeliohinta());
		
		double summa = Math.round((pHinta + pintakasittelyKokonaishinta) * 100.0) / 100.0;
		
		// hionta?
		if (hiontaCheckBox.isSelected()) {
			lisapalveluDao = new LisapalveluDao();
			hiontaOlio = lisapalveluDao.hae(846);
			summa = summa + hiontaOlio.getHinta();
		}
		else {
		}
		
		// Lisapalvelun hinta mukaan, lasketaan joko kertahinta tai tuntihinta
		if (valittuLisapalvelu != null) {
			if(valittuLisapalvelu.getTuntihinta() != 0) {
				double hinta = valittuLisapalvelu.getTuntihinta() * (valittuLisapalvelu.getAsetteen_tekeminen()/60);
				summa = summa +hinta;
				System.out.println("asetehinta:" +hinta);
			}
			else if (valittuLisapalvelu.getTuntihinta() == 0) {
				summa = summa + valittuLisapalvelu.getHinta();
				System.out.println("normihinta: "+valittuLisapalvelu.getHinta());
			}
		}
		kokonaisHinta.setText(Double.toString(summa));
	
		}
	}
	
	@FXML
	void peruutaKappaleenLisays(ActionEvent event) throws SQLException {
		
		PaaikkunaController pc = new PaaikkunaController();
		pc.avaaTuoteLisays(event);
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.close();
	}

	@FXML
	void avaaLisaaPintakasittelyaineIkkuna(ActionEvent event) throws SQLException, IOException{
		
		// loaderin alustus
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaTuotePintakasittelyaine.fxml"));
		loader.load();
		
		// Valitetaan LisaaTuotePintakasittelyaineControllerille nykyiset tiedot, jotta ne voidaan 
		// pintakasittelyn lisays -ikkunasta poistuessa settaa takaisin vanhoiksi arvoiksi.
		
		LisaaTuotePintakasittelyaineController controller = loader.getController();
		if (valittuPuutavara != null) {
			controller.setValittu_id(table.getSelectionModel().getSelectedIndex());
			controller.setValittu_nimi(kappaleNimiTxt.getText());
			controller.setValittu_hinta(Double.parseDouble(kokonaisHinta.getText()));
			controller.setValittu_syvyys(Double.parseDouble(syvyysTextBox.getText()));
			controller.setValittu_korkeus(Double.parseDouble(korkeusTextBox.getText()));
			controller.setValittu_leveys(Double.parseDouble(leveysTextBox.getText()));
			controller.setValittuLisapalvelu(lisamaksuComboBox.getSelectionModel().getSelectedItem());
			
			if(hiontaCheckBox.isSelected()) {
				controller.setHiotaanko(true);
			}
			else {
				controller.setHiotaanko(false);
			}
			System.out.println(syvyysTextBox.getText());
			if(Double.parseDouble(syvyysTextBox.getText()) == 0 || Double.parseDouble(syvyysTextBox.getText()) == 0.0|| syvyysTextBox.getText() == "") {
				controller.getSyvyysCheckBox().setDisable(true);
			}
			else {
				controller.getSyvyysCheckBox().setDisable(false);
			}
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Lisää pintakäsittelyaine kappaleelle");
			stage.setScene(new Scene(root, 940, 770));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.close();
		}
	}

	@FXML
	void lisaa(ActionEvent event) throws NumberFormatException, SQLException {
		
		//valiaikaisesti lisamaksun id:ksi 1, oikea toteutus myohemmin
		//Dao
		akp = new ArkistoKappaleDao();
		
		uusiKappale = new ArkistoKappale(1, kappaleNimiTxt.getText(), Double.parseDouble(leveysTextBox.getText()), Double.parseDouble(korkeusTextBox.getText()),
				Double.parseDouble(syvyysTextBox.getText()), Double.parseDouble(kokonaisHinta.getText()), pkasittelyaineOlio.getId(), valittuPuutavara.getId(),
				levitystapaOlio.getId(), valittuLisapalvelu.getId(), variTxt.getText(), kokoTxt.getText());
		
		// Varmistetaan, etta kayttoliittymassa on kaikki tiedot syotetty (tassa voi olla virheita, TODO varmistus myohemmin
		// ja lisataan tietokantaan
		if (kappaleNimiTxt.getText() != null && valittuPuutavara != null && pkasittelyaineOlio != null && levitystapaOlio != null) {
			if (akp.lisaa(uusiKappale)) {
				System.out.println("Onnistui");
			}
			
		}
		else {
			System.out.println("Epaonnistui");
		}
		
		
		//lisätään kappale tuotteen luonnin esikatseluikkunaan
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
		
		try {
			
			loader.load();
			LuoTuoteController controller = loader.getController();
			controller.setValittuKappale(uusiKappale);
			controller.getListItems().add(kappaleNimiTxt.getText());
			controller.getListItems().add("Koko: " + kokoTxt.getText());
			controller.getListItems().add("Väri: " + variTxt.getText());
			controller.getListItems().add("Kappalehinta: " + kokonaisHinta.getText());
			controller.getListItems().add("");
			
		} catch (IOException e) {
			e.printStackTrace();

		}

		
		Parent root = loader.getRoot();
    	Stage stage = new Stage();
		stage.setTitle("Luo tuote");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
		
	}
	@FXML
	void poistaPintakasittely(ActionEvent event) {
		
		// Poistetaan kaikki viittaukset valittuun pintakasittelyyn eli hinta, oliot ja textfieldit.
		// Lisaksi miinustetaan kappaleen kokonaishinnasta pintakasittelyn hinta sen resetoimiseksi.
		// Myos uuden pintakasittelynapin disablointi poistetaan
		kokonaisHinta.setText(Double.toString(Double.parseDouble(kokonaisHinta.getText())-pintakasittelyKokonaishinta));
		pintakasittelyKokonaishinta = 0;
		setPkasittelyaineOlio(null);
		setLevitystapaOlio(null);
		lisaaPintakasittelyBtn.setDisable(false);
		pkasittelyTxt.setText("");
		levitystapaTxt.setText("");
		
    	if(valittuPuutavara.getNeliohinta() != 0) {
    		syvyysTextBox.setDisable(true);
    		syvyysTextBox.setText("0");
    		try {
				laskeHintaPintaAla();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    	}
    	if(valittuPuutavara.getKuutiohinta() != 0) {
    		syvyysTextBox.setDisable(false);
    		try {
				laskeHintaTilavuus();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	} 
		
	}
	
	@FXML
	void poistaLisamaksu(ActionEvent event) {
		lisamaksuComboBox.getSelectionModel().clearSelection();
		valittuLisapalvelu = null;
		
    	if(valittuPuutavara.getNeliohinta() != 0) {
    		syvyysTextBox.setDisable(true);
    		syvyysTextBox.setText("0");
    		try {
				laskeHintaPintaAla();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    	}
    	if(valittuPuutavara.getKuutiohinta() != 0) {
    		syvyysTextBox.setDisable(false);
    		try {
				laskeHintaTilavuus();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	} 
	}
	
	
	// GETTERIT JA SETTERIT
	public Pintakasittelyaine getPkasittelyaineOlio() {
		return pkasittelyaineOlio;
	}


	public void setPkasittelyaineOlio(Pintakasittelyaine pkasittelyaineOlio) {
		this.pkasittelyaineOlio = pkasittelyaineOlio;
	}


	public Levitystapa getLevitystapaOlio() {
		return levitystapaOlio;
	}


	public void setLevitystapaOlio(Levitystapa levitystapaOlio) {
		this.levitystapaOlio = levitystapaOlio;
	}


	public TextField getPkasittelyTxt() {
		return pkasittelyTxt;
	}


	public void setPkasittelyTxt(TextField pkasittelyTxt) {
		this.pkasittelyTxt = pkasittelyTxt;
	}


	public TextField getLevitystapaTxt() {
		return levitystapaTxt;
	}


	public void setLevitystapaTxt(TextField levitystapaTxt) {
		this.levitystapaTxt = levitystapaTxt;
	}


	public double getPintakasittelyKokonaishinta() {
		return pintakasittelyKokonaishinta;
	}


	public void setPintakasittelyKokonaishinta(double pintakasittelyKokonaishinta) {
		this.pintakasittelyKokonaishinta = pintakasittelyKokonaishinta;
	}


	public Puutavara getValittuPuutavara() {
		return valittuPuutavara;
	}


	public void setValittuPuutavara(Puutavara valittuPuutavara) {
		this.valittuPuutavara = valittuPuutavara;
	}


	public TableView<Puutavara> getTable() {
		return table;
	}


	public void setTable(TableView<Puutavara> table) {
		this.table = table;
	}


	public int getValittu_id() {
		return valittu_id;
	}


	public void setValittu_id(int valittu_id) {
		this.valittu_id = valittu_id;
	}


	public TextField getKokonaisHinta() {
		return kokonaisHinta;
	}


	public void setKokonaisHinta(TextField kokonaisHinta) {
		this.kokonaisHinta = kokonaisHinta;
	}


	public TextField getLeveysTextBox() {
		return leveysTextBox;
	}


	public void setLeveysTextBox(TextField leveysTextBox) {
		this.leveysTextBox = leveysTextBox;
	}


	public TextField getKorkeusTextBox() {
		return korkeusTextBox;
	}


	public void setKorkeusTextBox(TextField korkeusTextBox) {
		this.korkeusTextBox = korkeusTextBox;
	}


	public TextField getSyvyysTextBox() {
		return syvyysTextBox;
	}


	public void setSyvyysTextBox(TextField syvyysTextBox) {
		this.syvyysTextBox = syvyysTextBox;
	}


	public TextField getKappaleNimiTxt() {
		return kappaleNimiTxt;
	}


	public void setKappaleNimiTxt(TextField kappaleNimiTxt) {
		this.kappaleNimiTxt = kappaleNimiTxt;
	}


	public CheckBox getLiimalevyCheckBox() {
		return liimalevyCheckBox;
	}


	public void setLiimalevyCheckBox(CheckBox liimalevyCheckBox) {
		this.liimalevyCheckBox = liimalevyCheckBox;
	}


	public double getValittu_leveys() {
		return valittu_leveys;
	}


	public void setValittu_leveys(double valittu_leveys) {
		this.valittu_leveys = valittu_leveys;
	}


	public double getValittu_korkeus() {
		return valittu_korkeus;
	}


	public void setValittu_korkeus(double valittu_korkeus) {
		this.valittu_korkeus = valittu_korkeus;
	}


	public double getValittu_syvyys() {
		return valittu_syvyys;
	}


	public void setValittu_syvyys(double valittu_syvyys) {
		this.valittu_syvyys = valittu_syvyys;
	}


	public double getValittu_hinta() {
		return valittu_hinta;
	}


	public void setValittu_hinta(double valittu_hinta) {
		this.valittu_hinta = valittu_hinta;
	}


	public Button getLisaaPintakasittelyBtn() {
		return lisaaPintakasittelyBtn;
	}


	public void setLisaaPintakasittelyBtn(Button lisaaPintakasittelyBtn) {
		this.lisaaPintakasittelyBtn = lisaaPintakasittelyBtn;
	}


	public LisaaKappaleController getTama() {
		return tama;
	}


	public void setTama(LisaaKappaleController tama) {
		this.tama = tama;
	}


	public CheckBox getHiontaCheckBox() {
		return hiontaCheckBox;
	}


	public void setHiontaCheckBox(CheckBox hiontaCheckBox) {
		this.hiontaCheckBox = hiontaCheckBox;
	}


	public ComboBox<Lisapalvelu> getLisamaksuComboBox() {
		return lisamaksuComboBox;
	}


	public void setLisamaksuComboBox(ComboBox<Lisapalvelu> lisamaksuComboBox) {
		this.lisamaksuComboBox = lisamaksuComboBox;
	}


	public boolean isHiotaanko() {
		return hiotaanko;
	}


	public void setHiotaanko(boolean hiotaanko) {
		this.hiotaanko = hiotaanko;
	}
	
	
}