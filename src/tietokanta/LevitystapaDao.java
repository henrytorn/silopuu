package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Levitystapa;


/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet liitosten lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class LevitystapaDao implements Dao<Levitystapa> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan liitoksen id
	 */
	@Override
	public Levitystapa hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu liitos haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Levitystapa WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Levitystapaa ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return levitystapaOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	
	/**
	 * Palauttaa tietokannasta kaikki liitokset-taulun alkiot 
	 * 
	 * @return	lista kaikista liitokset-taulun alkioista
	 */
	@Override
	public List<Levitystapa> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu liitos haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Levitystapa";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon liitos-oliot tallennetaan
			List<Levitystapa> levitystavat = new ArrayList<Levitystapa>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi liitos-olio apumetodin liitosOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Levitystapa levitystapaOlio = levitystapaOlioResultSetista(rs);
				levitystavat.add(levitystapaOlio);
			}
			
			// Palautetaan lista
			return levitystavat;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		

		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Liitos-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	liitos-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Levitystapa p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO levitystapa "
				+ "(nimi, perusmaksu, aika, tuntihinta) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getPerusmaksu());
			ps.setDouble(3, p.getAika());
			ps.setDouble(4, p.getTuntihinta());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Levitystapa p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE levitystapa SET "
				+ "nimi=?, perusmaksu=?, aika=?, tuntihinta=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getPerusmaksu());
			ps.setDouble(3, p.getAika());
			ps.setDouble(4, p.getTuntihinta());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM levitystapa WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Liitos-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Liitos-luokan olioksi
	 * @return		Liitos-luokan olio
	 * @throws SQLException
	 */
	public Levitystapa levitystapaOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Liitos malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Levitystapa levitystapaOlio = new Levitystapa();
		levitystapaOlio.setId(rs.getInt("id"));
		levitystapaOlio.setNimi(rs.getString("nimi"));
		levitystapaOlio.setPerusmaksu(rs.getDouble("perusmaksu"));
		levitystapaOlio.setAika(rs.getDouble("aika"));
		levitystapaOlio.setTuntihinta(rs.getDouble("tuntihinta"));
		
		return levitystapaOlio;	
	}
		
	
}
