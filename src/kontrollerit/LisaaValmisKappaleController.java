package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Valmiskappale;
import tietokanta.ValmiskappaleDao;

public class LisaaValmisKappaleController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField nimi;
	
	@FXML
	private TextField kappalehinta;
	
	@FXML
	private TextField koko;
	
	@FXML
	private TextField vari;
	
	@FXML
	public Button tallennaValmiskappaleBtn;
	
	@FXML
	public Button peruutaBtn;
	
	private int peruutaTesti;
	
	
	@FXML
	void peruutaLisays(ActionEvent event) throws SQLException {
		
	
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		// Lisatty valiaikainen (jonkun taytyy toteuttaa koko ikkunalle oikea toiminnallisuus) ehto, 
		// koska muuten Luo Kappale kohdassa avautuu peruuta nappia painaessa vaara ikkuna -Miikka
		if (peruutaTesti == 1) {
			TietokantaController tc = new TietokantaController();
			tc.avaaValmiskappaleIkkuna(event);
		}
		else if (peruutaTesti == 0) {
			PaaikkunaController pc = new PaaikkunaController();
			pc.avaaTuoteLisays(event);
		}
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaValmiskappale(ActionEvent event) throws SQLException {
		
		// Uusi liitos-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Valmiskappale ValmiskappaleOlio = new Valmiskappale();
		ValmiskappaleOlio.setNimi(nimi.getText());
		ValmiskappaleOlio.setKappalehinta(Double.parseDouble(kappalehinta.getText()));
		ValmiskappaleOlio.setKoko(Double.parseDouble(koko.getText()));
		ValmiskappaleOlio.setVari(vari.getText());
		
		// LiitosDao-luokan metodilla lisays tietokantaan
		ValmiskappaleDao p = new ValmiskappaleDao();
		if (p.lisaa(ValmiskappaleOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaValmiskappaleIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public int getPeruutaTesti() {
		return peruutaTesti;
	}

	public void setPeruutaTesti(int peruutaTesti) {
		this.peruutaTesti = peruutaTesti;
	}
	
}