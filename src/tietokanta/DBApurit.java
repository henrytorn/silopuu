package tietokanta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBApurit {

	// Metodi tietokannan avaukseen, palauttaa tietokantayhteyden.
	// Tassa projektissa talla hetkella tietokannan url ja kayttajatiedot maaritelty alla.
	public Connection yhdistaDb() {
		final String db = "jdbc:hsqldb:file:database/database;hsqldb.lock_file=false;ifexists=true";
		final String user = "SA";
		final String password = "silopuu"; 
		Connection conn = null;

        try {
            
            conn = DriverManager.getConnection(db, user, password);
        }
        catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        
        return conn;
	}
	
	// Overloadatut metodit tietokannan sulkemiseen
	public void suljeDb(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				
			}		
		}
	}
	public void suljeDb(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {	
			}	
		}
	}
	public void suljeDb(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {	
			}
		}
	}
}
