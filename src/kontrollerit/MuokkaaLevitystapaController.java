package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Levitystapa;
import tietokanta.LevitystapaDao;

public class MuokkaaLevitystapaController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField LevitystapaNimiTxt;
	
	@FXML
	private TextField LevitystapaPerusmaksuTxt;
	
	@FXML
	private TextField LevitystapaAikaTxt;
	
	@FXML
	private TextField LevitystapaTuntihintaTxt;
	
	@FXML
	private Button tallennaLevitystapaBtn;
	
	@FXML
	private Button peruutaBtn;
	
	private Levitystapa valittuLevitystapa;
	
	@FXML
	void peruutaLevitystapaMuokkaus (ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLevitystapaIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void muokkaaLevitystapa(ActionEvent event) throws SQLException {
		
		Levitystapa levitystapaOlio = new Levitystapa();
		
		// Tassa manuaalisesti laitettu id testitarkoitukseen, silla
		// kayttoliittymassa ei ole mahdollisuutta viela valita haluttua
		// tietuetta eli id:ta
		levitystapaOlio.setNimi(LevitystapaNimiTxt.getText());
		levitystapaOlio.setPerusmaksu(Double.parseDouble(LevitystapaPerusmaksuTxt.getText()));
		levitystapaOlio.setAika(Double.parseDouble(LevitystapaAikaTxt.getText()));
		levitystapaOlio.setTuntihinta(Double.parseDouble(LevitystapaTuntihintaTxt.getText()));
		levitystapaOlio.setId(valittuLevitystapa.getId());
		
		LevitystapaDao p = new LevitystapaDao();
		if (p.paivita(levitystapaOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
		
		TietokantaController tc = new TietokantaController();
		tc.avaaLevitystapaIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public TextField getLevitystapaNimiTxt() {
		return LevitystapaNimiTxt;
	}

	public void setLevitystapaNimiTxt(TextField LevitystapaNimiTxt) {
		this.LevitystapaNimiTxt = LevitystapaNimiTxt;
	}

	public TextField getLevitystapaPerusmaksuTxt() {
		return LevitystapaPerusmaksuTxt;
	}

	public void setLevitystapaPerusmaksuTxt(TextField LevitystapaPerusmaksuTxt) {
		this.LevitystapaPerusmaksuTxt = LevitystapaPerusmaksuTxt;
	}

	public TextField getLevitystapaAikaTxt() {
		return LevitystapaAikaTxt;
	}

	public void setLevitystapaAikaTxt(TextField LevitystapaAikaTxt) {
		this.LevitystapaAikaTxt = LevitystapaAikaTxt;
	}
	
	public TextField getLevitystapaTuntihintaTxt() {
		return LevitystapaTuntihintaTxt;
	}

	public void setLevitystapaTuntihintaTxt(TextField LevitystapaTuntihintaTxt) {
		this.LevitystapaTuntihintaTxt = LevitystapaTuntihintaTxt;
	}

	public Levitystapa getValittuLevitystapa() {
		return valittuLevitystapa;
	}

	public void setValittuLevitystapa(Levitystapa valittuLevitystapa) {
		this.valittuLevitystapa = valittuLevitystapa;
	}
	
}
