package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Puutavara;
import mallit.Tarvike;
import mallit.Liitos;
import mallit.Valmiskappale;
import tietokanta.DBApurit;

public class LisaaTuoteLiitosController implements Initializable{
	
	@FXML 
	private TableView<Liitos> table;
	
	@FXML 
	private TableColumn<Liitos, Integer> id;
	
	@FXML 
	private TableColumn<Liitos, String> nimi;
	
	@FXML 
	private TableColumn<Liitos, Double> kappalehinta;
	
	@FXML 
	private TableColumn<Liitos, Double> tuntihinta;
	
	@FXML 
	private TableColumn<Liitos, Double> asetteen_tekeminen;

	@FXML 
	private Button tallennaLiitosBtn;
	
	@FXML 
	private Button peruutaLiitosBtn;

	@FXML 
	private TextField kokonaisHinta;
	
	@FXML
	private Spinner liitosmaaraSpinner;
	
	private LuoTuoteController tama;
	ObservableList<Liitos> Liitoslist = FXCollections.observableArrayList();
	
	
	@FXML
	void tallennaLiitos(ActionEvent event) throws SQLException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
		
		int maara = (Integer) liitosmaaraSpinner.getValue();
		
		try {
			
			loader.load();
			LuoTuoteController controller = loader.getController();
			Liitos valittuLiitos = table.getSelectionModel().getSelectedItem();

			if (valittuLiitos != null) {
				controller.setValittuLiitos(valittuLiitos);
				controller.setValittuTarvike(tama.getValittuTarvike());
				controller.setValittuKappale(tama.getValittuKappale());
				controller.getListItems().add(valittuLiitos.getNimi());
				controller.getListItems().add("- kappalehinta: " + Double.toString(valittuLiitos.getKappalehinta()) + " €");
				controller.getListItems().add("- Määrä: " + Integer.toString((int) liitosmaaraSpinner.getValue()));
				controller.getListItems().add("- Asetteen tekeminen: " + Double.toString(valittuLiitos.getAsetteen_tekeminen()) + " min");
				controller.getListItems().add("- Tuntihinta: " + Double.toString(valittuLiitos.getTuntihinta()) + " €");
				controller.getListItems().add("- Kokonaishinta: " + laskeHinta());
				controller.getListItems().add("");
				
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Luo tuote");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	
	@FXML
	void peruutaLiitos(ActionEvent event) throws SQLException {


		PaaikkunaController controller = new PaaikkunaController();
		controller.avaaTuoteLisays(event);

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	    System.out.print("peruutettu");
	            	
	}
	
	private String laskeHinta() {

		Liitos valittuLiitos = table.getSelectionModel().getSelectedItem();
		
		int maara = (int) liitosmaaraSpinner.getValue();
		double kappalehinta = valittuLiitos.getKappalehinta();
		double tuntihinta = valittuLiitos.getTuntihinta();
		double asetteen_tekeminen = valittuLiitos.getAsetteen_tekeminen();
		double yhteishinta = (kappalehinta * maara + (tuntihinta / 60 * asetteen_tekeminen));
		System.out.println(yhteishinta);		
		
		String tulos = String.valueOf(yhteishinta);
		kokonaisHinta.setText(tulos);
		
		return tulos;
		}
	        	    
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Liitokset";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Liitoslist.add(new Liitos(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("tuntihinta"), rs.getDouble("asetteen_tekeminen")));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
	
		id.setCellValueFactory(new PropertyValueFactory<Liitos, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Liitos, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("kappalehinta"));
		tuntihinta.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("tuntihinta"));
		asetteen_tekeminen.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("asetteen_tekeminen"));
		table.setItems(Liitoslist);
		
		//tablen listener
		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				laskeHinta();
			}
			
			
	    });
		
		liitosmaaraSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue != oldValue) {
				laskeHinta();
			}
		});
		
		
		
		
	}


	public LuoTuoteController getTama() {
		return tama;
	}


	public void setTama(LuoTuoteController tama) {
		this.tama = tama;
	}
		
		
			
		



}