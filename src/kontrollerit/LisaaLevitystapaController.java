package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Levitystapa;
import tietokanta.LevitystapaDao;

public class LisaaLevitystapaController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField LevitystapaNimiTxt;
	
	@FXML
	private TextField LevitystapaPerusmaksuTxt;
	
	@FXML
	private TextField LevitystapaAikaTxt;
	
	@FXML
	public Button tallennaLevitystapaBtn;
	
	@FXML
	public Button peruutaKappaleenlisaysBtn;
	
	@FXML
	private TextField LevitystapaTuntihintaTxt;
	
	@FXML
	void peruutaLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLevitystapaIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaLevitystapa(ActionEvent event) throws SQLException {
		
		// Uusi liitos-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Levitystapa levitystapaOlio = new Levitystapa();
		levitystapaOlio.setNimi(LevitystapaNimiTxt.getText());
		levitystapaOlio.setPerusmaksu(Double.parseDouble(LevitystapaPerusmaksuTxt.getText()));
		levitystapaOlio.setAika(Double.parseDouble(LevitystapaAikaTxt.getText()));
		levitystapaOlio.setTuntihinta(Double.parseDouble(LevitystapaTuntihintaTxt.getText()));
		
		// LiitosDao-luokan metodilla lisays tietokantaan
		LevitystapaDao p = new LevitystapaDao();
		if (p.lisaa(levitystapaOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaLevitystapaIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
