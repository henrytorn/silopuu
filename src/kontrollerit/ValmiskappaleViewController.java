package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Valmiskappale;
import tietokanta.DBApurit;
import tietokanta.ValmiskappaleDao;

public class ValmiskappaleViewController implements Initializable{

	@FXML 
	private TableView<Valmiskappale> table;
	
	@FXML 
	private TableColumn<Valmiskappale, Integer> id;
	
	@FXML 
	private TableColumn<Valmiskappale, String> nimi;
	
	@FXML 
	private TableColumn<Valmiskappale, Double> kappalehinta;
	
	@FXML 
	private TableColumn<Valmiskappale, Double> koko;
	
	@FXML 
	private TableColumn<Valmiskappale, String> vari;
	
	@FXML
	public Button lisaaBtn;
	
	@FXML
	public TextField hakuTBox;
	
	ObservableList<Valmiskappale> Valmiskappalelist = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Valmis_kappale";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Valmiskappalelist.add(new Valmiskappale(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("koko"), rs.getString("vari")));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<Valmiskappale, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Valmiskappale, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<Valmiskappale, Double>("kappalehinta"));
		koko.setCellValueFactory(new PropertyValueFactory<Valmiskappale, Double>("koko"));
		vari.setCellValueFactory(new PropertyValueFactory<Valmiskappale, String>("vari"));
		table.setItems(Valmiskappalelist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeValmiskappale(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	void avaaLisaaIkkuna(ActionEvent event) throws SQLException, IOException {
		
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaValmisKappale.fxml"));
		loader.load();
		
		LisaaValmisKappaleController controller = loader.getController();
		controller.setPeruutaTesti(1);
		
		Parent root = loader.getRoot();
    	Stage stage = new Stage();
		stage.setTitle("Muokkaa");
		stage.setScene(new Scene(root, 365, 410));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void avaaMuokkaaIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaValmisKappale.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja Valmiskappale-olion valinta tableviewista
		MuokkaaValmiskappaleController controller = loader.getController();
    	Valmiskappale valittuValmiskappale = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin Valmiskappale, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuValmiskappale != null) {
			controller.setValittuValmiskappale(valittuValmiskappale);
			controller.getValmiskappaleKappalehintaTxt().setText(Double.toString(valittuValmiskappale.getKappalehinta()));
			controller.getValmiskappaleKokoTxt().setText(Double.toString(valittuValmiskappale.getKoko()));
			controller.getValmiskappaleVariTxt().setText(valittuValmiskappale.getVari());
			controller.getValmiskappaleNimiTxt().setText(valittuValmiskappale.getNimi());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	@FXML
	void poistaValmiskappale(ActionEvent event) throws SQLException {

    	Valmiskappale valittuValmiskappale = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa kappaleen " + valittuValmiskappale.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) { 
    	
    		if (valittuValmiskappale != null) {
    			try {
    				ValmiskappaleDao dao = new ValmiskappaleDao();
    				if (dao.poista(valittuValmiskappale.getId())) {
    					System.out.println("Poistettiin Valmiskappale: " + valittuValmiskappale.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			Valmiskappalelist.removeAll(Valmiskappalelist);
    			initialize(null, null);
    		}
    	}
		
	}
		
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void haeValmiskappale(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Valmis_kappale WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			Valmiskappalelist.clear();

			while (rs.next()) {
				Valmiskappalelist.add(new Valmiskappale(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("koko"),rs.getString("vari")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}