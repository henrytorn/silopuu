package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Pintakasittelyaine;
import tietokanta.PintakasittelyaineDao;

public class MuokkaaPintakasittelyaineController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField pintakasittelyaineNimiTxt;
	
	@FXML
	private TextField pintakasittelyaineHintaTxt;
	
	@FXML
	private TextField pintakasittelyaineRiittoisuusTxt;
	
	@FXML
	private TextField pintakasittelyaineTyomaaraTxt;
	
	@FXML
	public Button tallennaPintakasittelyaineBtn;
	
	@FXML
	public Button peruutaBtn;
	
	private Pintakasittelyaine valittuPintakasittelyaine;

	@FXML
	void peruutaMuokkaus(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaPintakasittelyaineetIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void muokkaaPintakasittelyaine(ActionEvent event) throws SQLException {
		
		Pintakasittelyaine pintakasittelyaineOlio = new Pintakasittelyaine();
		System.out.println(valittuPintakasittelyaine.toString());

		pintakasittelyaineOlio.setNimi(pintakasittelyaineNimiTxt.getText());
		pintakasittelyaineOlio.setLitrahinta(Double.parseDouble(pintakasittelyaineHintaTxt.getText()));
		pintakasittelyaineOlio.setRiittoisuus(Double.parseDouble(pintakasittelyaineRiittoisuusTxt.getText()));
		pintakasittelyaineOlio.setId(valittuPintakasittelyaine.getId());
		
		PintakasittelyaineDao p = new PintakasittelyaineDao();
		if (p.paivita(pintakasittelyaineOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
		
		TietokantaController tc = new TietokantaController();
		tc.avaaPintakasittelyaineetIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public TextField getPintakasittelyaineNimiTxt() {
		return pintakasittelyaineNimiTxt;
	}

	public void setPintakasittelyaineNimiTxt(TextField pintakasittelyaineNimiTxt) {
		this.pintakasittelyaineNimiTxt = pintakasittelyaineNimiTxt;
	}

	public TextField getPintakasittelyaineHintaTxt() {
		return pintakasittelyaineHintaTxt;
	}

	public void setPintakasittelyaineHintaTxt(TextField pintakasittelyaineHintaTxt) {
		this.pintakasittelyaineHintaTxt = pintakasittelyaineHintaTxt;
	}

	public TextField getPintakasittelyaineRiittoisuusTxt() {
		return pintakasittelyaineRiittoisuusTxt;
	}

	public void setPintakasittelyaineRiittoisuusTxt(TextField pintakasittelyaineRiittoisuusTxt) {
		this.pintakasittelyaineRiittoisuusTxt = pintakasittelyaineRiittoisuusTxt;
	}

	public TextField getPintakasittelyaineTyomaaraTxt() {
		return pintakasittelyaineTyomaaraTxt;
	}

	public void setPintakasittelyaineTyomaaraTxt(TextField pintakasittelyaineTyomaaraTxt) {
		this.pintakasittelyaineTyomaaraTxt = pintakasittelyaineTyomaaraTxt;
	}

	public Pintakasittelyaine getValittuPintakasittelyaine() {
		return valittuPintakasittelyaine;
	}

	public void setValittuPintakasittelyaine(Pintakasittelyaine valittuPintakasittelyaine) {
		this.valittuPintakasittelyaine = valittuPintakasittelyaine;
	}
	
	
}
