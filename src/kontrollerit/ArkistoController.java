package kontrollerit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ArkistoController {

	
	@FXML
	void avaaTiedostotIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoTiedostot.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Tiedostot");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaLuoPdfIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoLuoPdf.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("PDF");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
		
	}
	
	@FXML
	void avaaTuotteetIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoTuotteet.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Tuotteet");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
		
	}
	
	@FXML
	void avaaKappaleetIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoKappaleet.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Kappaleet");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
		
	}
	
	

}