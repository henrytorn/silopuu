package mallit;

public class Lisapalvelu {
	
	private int id;
	private String nimi;
	private double hinta;
	private double tuntihinta;
	private double asetteen_tekeminen;
	
	public Lisapalvelu() {
	}
	
	public Lisapalvelu(int id, String nimi, double hinta, double tuntihinta, double asetteen_tekemnen) {
		this.id = id;
		this.nimi = nimi;
		this.hinta = hinta;
		this.tuntihinta = tuntihinta;
		this.asetteen_tekeminen = asetteen_tekeminen;
		
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}


	public double getHinta() {
		return hinta;
	}

	public void setHinta(double hinta) {
		this.hinta = hinta;
	}

	public double getTuntihinta() {
		return tuntihinta;
	}

	public void setTuntihinta(double tuntihinta) {
		this.tuntihinta = tuntihinta;
	}

	public double getAsetteen_tekeminen() {
		return asetteen_tekeminen;
	}

	public void setAsetteen_tekeminen(double asetteen_tekeminen) {
		this.asetteen_tekeminen = asetteen_tekeminen;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Hinta: " + this.hinta;
	}
}
