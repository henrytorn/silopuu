package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Logo;



public class LogoDao implements Dao<Logo> {

	
	@Override
	public boolean paivita(Logo p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE asetukset SET " + "logo=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Logo hae(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Logo> haeKaikki() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean lisaa(Logo t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	
}
