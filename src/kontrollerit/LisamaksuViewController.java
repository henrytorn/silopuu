package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Liitos;
import mallit.Lisapalvelu;
import mallit.Puutavara;
import tietokanta.DBApurit;
import tietokanta.LisapalveluDao;
import kontrollerit.Apumetodeja;

public class LisamaksuViewController implements Initializable{

	@FXML private TableView<Lisapalvelu> table;
	@FXML private TableColumn<Lisapalvelu, Integer> id;
	@FXML private TableColumn<Lisapalvelu, String> nimi;
	@FXML private TableColumn<Lisapalvelu, Double> hinta;
	@FXML private TableColumn<Lisapalvelu, Double> tuntihinta;
	@FXML private TableColumn<Lisapalvelu, Double> asetteen_tekeminen;
	@FXML private Button lisaaLisamaksuBtn;
	ObservableList<Lisapalvelu> lisamaksulist = FXCollections.observableArrayList();
	private LisapalveluDao dao = new LisapalveluDao();
	private List<Lisapalvelu> lisapalveluLista;
	
	@FXML
	public TextField hakuTBox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			lisapalveluLista = dao.haeKaikki();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (Lisapalvelu x : lisapalveluLista) {
			lisamaksulist.add(x);
		}
				
		id.setCellValueFactory(new PropertyValueFactory<Lisapalvelu, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Lisapalvelu, String>("nimi"));
		hinta.setCellValueFactory(new PropertyValueFactory<Lisapalvelu, Double>("hinta"));
		tuntihinta.setCellValueFactory(new PropertyValueFactory<Lisapalvelu, Double>("tuntihinta"));
		asetteen_tekeminen.setCellValueFactory(new PropertyValueFactory<Lisapalvelu, Double>("asetteen_tekeminen"));
		
		table.setItems(lisamaksulist);
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeLisamaksut(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	

	@FXML
	void avaaLisaaLisamaksuIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaLisamaksu.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisaa lisamaksu");
    		stage.setScene(new Scene(root, 365, 410));
    		stage.show();
    		
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	
	@FXML
	void avaaMuokkaaLisamaksuIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaLisamaksu.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja olion valinta tableviewista
		MuokkaaLisamaksuController controller = loader.getController();
    	Lisapalvelu valittuLisamaksu = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin tietue taulusta, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuLisamaksu != null) {
			controller.setValittuLisamaksu(valittuLisamaksu);
			controller.getLisamaksuHintaTxt().setText(Double.toString(valittuLisamaksu.getHinta()));
			controller.getLisamaksuNimiTxt().setText(valittuLisamaksu.getNimi());
			controller.getLisamaksuAsetteenTekeminenTxt().setText(Double.toString(valittuLisamaksu.getAsetteen_tekeminen()));
			controller.getLisamaksuTuntihintaTxt().setText(Double.toString(valittuLisamaksu.getTuntihinta()));
	
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	

	@FXML
	void poistaLisamaksu(ActionEvent event) throws SQLException {

    	Lisapalvelu valittuLisamaksu = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa lisämaksun " + valittuLisamaksu.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) {
    	
    		if (valittuLisamaksu != null) {
    			try {
    				LisapalveluDao dao = new LisapalveluDao();
    				if (dao.poista(valittuLisamaksu.getId())) {
    					System.out.println("Poistettiin lisamaksu: " + valittuLisamaksu.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			lisamaksulist.removeAll(lisamaksulist);
    			initialize(null, null);
    		}
    	}
		
	}
	
	@FXML  
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void haeLisamaksut(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Lisapalvelut WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			lisamaksulist.clear();

			while (rs.next()) {
				lisamaksulist.add(new Lisapalvelu(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("hinta"), 
						rs.getDouble("tuntihinta"), rs.getDouble("asetteen_tekeminen")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}