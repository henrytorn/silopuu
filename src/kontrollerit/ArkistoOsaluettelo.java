package kontrollerit;

import java.awt.Button;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;



public class ArkistoOsaluettelo {
	


	@FXML
    void avaaLuoPdfIkkuna(ActionEvent event) throws SQLException {

        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoLuoPdf.fxml"));
            Stage stage = new Stage();
            stage.setTitle("PDF");
            stage.setScene(new Scene(root, 1280, 768));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
	

}