package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.ArkistoTuote;
import mallit.Levitystapa;
import mallit.Tarvike;
import mallit.Valmiskappale;
import tietokanta.DBApurit;
import tietokanta.TarvikeDao;
import tietokanta.ArkistoTuoteDao;

public class ArkistoTuotteet implements Initializable{

	@FXML 
	private TableView<ArkistoTuote> tuotteetTable;
	
	@FXML 
	private TableColumn<ArkistoTuote, Integer> id;
	
	@FXML 
	private TableColumn<ArkistoTuote, String> nimi;
	
	@FXML 
	private TableColumn<ArkistoTuote, Double> hinta;
	
	@FXML
	public Button lisaaBtn;
	
	@FXML
	public TextField hakuTBox;
	
	ObservableList<ArkistoTuote> Arkistotuotelist = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM tuotteet";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Arkistotuotelist.add(new ArkistoTuote(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("hinta")));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<ArkistoTuote, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<ArkistoTuote, String>("nimi"));
		hinta.setCellValueFactory(new PropertyValueFactory<ArkistoTuote, Double>("hinta"));
		tuotteetTable.setItems(Arkistotuotelist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeArkistoTuote(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
    void avaaLuoPdfIkkuna(ActionEvent event) throws SQLException {

        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoLuoPdf.fxml"));
            Stage stage = new Stage();
            stage.setTitle("PDF");
            stage.setScene(new Scene(root, 1280, 768));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
	
	@FXML
	void poistaTuote(ActionEvent event) throws SQLException {
		// TODO VAROITUSIKKUNA ENNEN POISTOA!!!
		ArkistoTuote valittuTuote = tuotteetTable.getSelectionModel().getSelectedItem();
    	if (valittuTuote != null) {
			try {
				ArkistoTuoteDao dao = new ArkistoTuoteDao();
				if (dao.poista(valittuTuote.getId())) {
					System.out.println("Poistettiin Tuote: " + valittuTuote.toString());
				}
				else {
					System.out.println("Poisto epaonnistui");
					//TODO parempi ilmoitus
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
	
			Arkistotuotelist.removeAll(Arkistotuotelist);
			initialize(null, null);
    	}
	}
	
	@FXML
	void avaaOsaluettelo(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoOsaluettelo.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Osaluettelo");
    		stage.setScene(new Scene(root, 1280, 768));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
		
	}
	
	// TODO MUOKKAUS TOIMIVAKSI, AVAA NYT LUO KAPPALE KOPION
	@FXML
	void avaaLuoTuoteKopio(ActionEvent event) throws IOException, SQLException {
		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LuoKappaleKopio.fxml"));
		loader.load();

		LuoKappaleKopioController controller = loader.getController();

		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Kappaleen kopiointi");
		stage.setScene(new Scene(root, 450, 600));
		stage.show();
		
		//Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		//currStage.close();
	}
	
	@FXML
	void haeArkistoTuote(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM tuotteet WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			Arkistotuotelist.clear();

			while (rs.next()) {
				Arkistotuotelist.add(new ArkistoTuote(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("hinta")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
}