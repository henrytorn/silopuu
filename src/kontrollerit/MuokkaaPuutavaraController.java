package kontrollerit;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Puutavara;
import tietokanta.PuutavaraDao;
import kontrollerit.TietokantaController;

public class MuokkaaPuutavaraController  {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField PuutavaraNimiTxt;
	
	@FXML
	private TextField PuutavaraHintaTxt;
	
	@FXML
	private TextField PuutavaraNelioHintaTxt;
	
	@FXML
	private TextField PuutavaraMassaTxt;
	
	private Puutavara valittuPuutavara;
	
	@FXML
	void muokkaaPuutavara(ActionEvent event) throws SQLException {
		Puutavara PuutavaraOlio = new Puutavara();
		
		PuutavaraOlio.setNimi(PuutavaraNimiTxt.getText());
		PuutavaraOlio.setKuutiohinta(Double.parseDouble(PuutavaraHintaTxt.getText()));
		PuutavaraOlio.setNeliohinta(Double.parseDouble(PuutavaraNelioHintaTxt.getText()));
		PuutavaraOlio.setTiheys(Double.parseDouble(PuutavaraMassaTxt.getText()));
		PuutavaraOlio.setId(getValittuPuutavara().getId());
		
		PuutavaraDao p = new PuutavaraDao();
		if (p.paivita(PuutavaraOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
	
		// Avataan edellinen ikkuna ja suljetaan nykyinen
		TietokantaController tc = new TietokantaController();
		tc.avaaPuutavaratIkkuna(event);
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void peruuta(ActionEvent event) throws SQLException {
		TietokantaController tc = new TietokantaController();
		tc.avaaPuutavaratIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public Puutavara getValittuPuutavara() {
		return valittuPuutavara;
	}

	public void setValittuPuutavara(Puutavara valittuPuutavara) {
		this.valittuPuutavara = valittuPuutavara;
	}

	public TextField getPuutavaraNimiTxt() {
		return PuutavaraNimiTxt;
	}

	public void setPuutavaraNimiTxt(TextField PuutavaraNimiTxt) {
		this.PuutavaraNimiTxt = PuutavaraNimiTxt;
	}

	public TextField getPuutavaraHintaTxt() {
		return PuutavaraHintaTxt;
	}


	public TextField getPuutavaraNelioHintaTxt() {
		return PuutavaraNelioHintaTxt;
	}

	public void setPuutavaraHintaTxt(TextField PuutavaraHintaTxt) {
		this.PuutavaraHintaTxt = PuutavaraHintaTxt;
	}
	public void setPuutavaraNelioHintaTxt(TextField PuutavaraNelioHintaTxt) {
		this.PuutavaraNelioHintaTxt = PuutavaraNelioHintaTxt;
	}

	public TextField getPuutavaraMassaTxt() {
		return PuutavaraMassaTxt;
	}

	public void setPuutavaraMassaTxt(TextField PuutavaraMassaTxt) {
		this.PuutavaraMassaTxt = PuutavaraMassaTxt;
	}
	
}
