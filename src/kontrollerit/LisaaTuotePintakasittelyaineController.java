package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import kontrollerit.LuoTuoteController;
import mallit.Levitystapa;
import mallit.Lisapalvelu;
import mallit.Pintakasittelyaine;
import mallit.Puutavara;
import tietokanta.DBApurit;
import tietokanta.LevitystapaDao;
import tietokanta.PintakasittelyaineDao;


// TODO - HUTTUIISTA KOODIA, SIISTIN KUHAN KERKEAN - Miikka
public class LisaaTuotePintakasittelyaineController implements Initializable{
	
	private PintakasittelyaineDao pDao = new PintakasittelyaineDao();
	private Pintakasittelyaine valittuPintakasittely;
	private Levitystapa valittuLevitystapa;
	private LevitystapaDao levitysDao = new LevitystapaDao();
	private LisaaKappaleController tama;
	
	private ObservableList<Pintakasittelyaine> pkasittelyaineList = FXCollections.observableArrayList();
	private ObservableList<Levitystapa> levitystapaList = FXCollections.observableArrayList();
	private List<Levitystapa> haetutLevitystavat;
	private List<Pintakasittelyaine> haetutPintakasittelyaineet;

	private int levitystapa_id;

	private double pintaala;
	private double toimenpidehinta;
	private double kokonaishinta;
	private double aineen_kulutus;
	private double aineen_hinta;
	private int kasittelykerrat;
	private int sivujen_lkm;
	private TableView<Puutavara> table;
	private int valittu_id;
	private double valittu_leveys, valittu_korkeus, valittu_syvyys, valittu_hinta;
	private String valittu_nimi;
	private boolean kaikkiPinnatValittu;
	
	@FXML
	private TableView<Pintakasittelyaine> pintakasittelyTableView;
	
	@FXML
	private TableColumn<Pintakasittelyaine, String> pintakasittelyNimiClmn;
	
	@FXML
	private TableColumn<Pintakasittelyaine, Double> pintakasittelyHintaClmn;
	
	@FXML
	private TableColumn<Pintakasittelyaine, Double> pintakasittelyRiittoisuusClmn;
	
	@FXML
	private TableView<Levitystapa> levitystapaTableView;
	
	@FXML
	private TableColumn<Levitystapa, String> levitystapaNimiClmn;
	
	@FXML
	private TableColumn<Levitystapa, Double> levitystapaPerusmaksuClmn;
	
	@FXML
	private TableColumn<Levitystapa, Double> levitystapaAikaClmn;
	
	@FXML
	private TableColumn<Levitystapa, Double> levitystapaTuntihintaClmn;
	
	@FXML
	private Spinner<Integer> kasittelykerratSpinner;
	@FXML
	private Spinner<Integer> sivujenKasittelymaaraSpinner;
	
	@FXML
	private TextField kokonaishintaTxt;
	
	@FXML
	private Button peruutaBtn;
	
	@FXML
	private CheckBox syvyysCheckBox;
	
	@FXML
	private CheckBox leveysCheckBox;
	
	@FXML
	private CheckBox korkeusCheckBox;
	
	private Lisapalvelu valittuLisapalvelu;
	private boolean hiotaanko;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setPintakasittelyLista();
		pintakasittelyNimiClmn.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, String>("nimi"));
		pintakasittelyHintaClmn.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, Double>("litrahinta"));
		pintakasittelyRiittoisuusClmn.setCellValueFactory(new PropertyValueFactory<Pintakasittelyaine, Double>("riittoisuus"));

		pintakasittelyTableView.setItems(pkasittelyaineList);
		
		setLevitystapaLista();
		levitystapaNimiClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, String>("nimi"));
		levitystapaPerusmaksuClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("perusmaksu"));
		levitystapaAikaClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("aika"));
		levitystapaTuntihintaClmn.setCellValueFactory(new PropertyValueFactory<Levitystapa, Double>("tuntihinta"));

		levitystapaTableView.setItems(levitystapaList);
//		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
//			try {
//				haeLiitokset(conn, newText);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		});
		
		
		// Kuuntelijat kayttoliittyman elementeille
		pintakasittelyTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			
			// resettaukset, kun aloitetaan uusien pintakasittelyjen ja levitystapojen valinta
			kasittelykerratSpinner.getValueFactory().setValue(1);
			sivujenKasittelymaaraSpinner.getValueFactory().setValue(1);
			kokonaishintaTxt.clear();
			levitystapaTableView.getSelectionModel().clearSelection();
			if (newSelection != null) {
				
				// tyhjennetaan tiedot aina, kun lahdetaan alusta valitsemaan pintakasittelya ja levitystapaa
				valittuLevitystapa = null;
				
				setValittuPintakasittely(newSelection);
			}
		});
		
		levitystapaTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null & valittuPintakasittely != null) {
				
				kasittelykerratSpinner.getValueFactory().setValue(1);
				sivujenKasittelymaaraSpinner.getValueFactory().setValue(1);
				setValittuLevitystapa(newSelection);
				
				kasittelykerrat = kasittelykerratSpinner.getValue();
				sivujen_lkm = sivujenKasittelymaaraSpinner.getValue();
				setPintaala(laskePintaala(valittu_korkeus, valittu_leveys, valittu_syvyys));
				setToimenpidehinta(laskeToimenpiteenHinta(valittuLevitystapa.getAika(), valittuLevitystapa.getTuntihinta(), 
						valittuLevitystapa.getPerusmaksu()));
				setAineen_kulutus(laskeAineenKulutus(valittuPintakasittely.getRiittoisuus(), getPintaala()));
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), kasittelykerrat));

				
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
				
			}
		});
		
		kasittelykerratSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			kasittelykerrat = newValue;
			sivujen_lkm = sivujenKasittelymaaraSpinner.getValue();
			if (valittuPintakasittely != null) {
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), newValue));
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
			}
		});

		
		sivujenKasittelymaaraSpinner.valueProperty().addListener((obs, oldValue, newValue)-> {
			sivujen_lkm = newValue;
			kasittelykerrat = kasittelykerratSpinner.getValue();
			if (valittuPintakasittely !=null) {
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), kasittelykerrat));
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
			}
		});
		
		korkeusCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				setPintaala(laskePintaala(valittu_korkeus, valittu_leveys, valittu_syvyys));
				setToimenpidehinta(laskeToimenpiteenHinta(valittuLevitystapa.getAika(), valittuLevitystapa.getTuntihinta(), 
						valittuLevitystapa.getPerusmaksu()));
				setAineen_kulutus(laskeAineenKulutus(valittuPintakasittely.getRiittoisuus(), getPintaala()));
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), kasittelykerrat));
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
			}
		});
		
		syvyysCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				setPintaala(laskePintaala(valittu_korkeus, valittu_leveys, valittu_syvyys));
				setToimenpidehinta(laskeToimenpiteenHinta(valittuLevitystapa.getAika(), valittuLevitystapa.getTuntihinta(), 
						valittuLevitystapa.getPerusmaksu()));
				setAineen_kulutus(laskeAineenKulutus(valittuPintakasittely.getRiittoisuus(), getPintaala()));
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), kasittelykerrat));
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
			}
		});
		
		leveysCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				setPintaala(laskePintaala(valittu_korkeus, valittu_leveys, valittu_syvyys));
				setToimenpidehinta(laskeToimenpiteenHinta(valittuLevitystapa.getAika(), valittuLevitystapa.getTuntihinta(), 
						valittuLevitystapa.getPerusmaksu()));
				setAineen_kulutus(laskeAineenKulutus(valittuPintakasittely.getRiittoisuus(), getPintaala()));
				setAineen_hinta(laskeAineenHinta(getAineen_kulutus(), valittuPintakasittely.getLitrahinta(), kasittelykerrat));
				paivitaPintakasittelyTiedot(valittuPintakasittely, valittuLevitystapa, kokonaishintaTxt);
			}
		}); //kuuntelijat
	}
	

	// Metodeja laskemiseen yms
	private double laskePintaala(double korkeus, double leveys, double syvyys) {
		
		if (korkeusCheckBox.isSelected() && leveysCheckBox.isSelected()) {
			return ((korkeus/1000)*(leveys/1000));
		}
		else if (korkeusCheckBox.isSelected() && syvyysCheckBox.isSelected()) {
			return ((korkeus/1000)*(syvyys/1000));
		}
		else if (leveysCheckBox.isSelected() && syvyysCheckBox.isSelected()) {
			return ((leveys/1000)*(syvyys/1000));
		}
		else if (leveysCheckBox.isSelected() && syvyysCheckBox.isSelected() && korkeusCheckBox.isSelected()) {
			kaikkiPinnatValittu = true;
			//TODO TOTEUTA KAIKKIEN PINTOJEN VALINNALLE JOKIN JARKEVA TOIMINNALLISUUS
		}
		return 0;
	}
	
	private double laskeAineenKulutus(double riittoisuus, double p_ala) {
		return p_ala/riittoisuus;
	}
	
	private double laskeAineenHinta (double kulutus, double litrahinta, int kasittelykerrat) {
			return (kulutus * litrahinta)*kasittelykerrat;

	}
	
	private double laskeToimenpiteenHinta(double levitysaika, double tuntihinta, double perusmaksu) {
		return ((levitysaika * tuntihinta) + perusmaksu);
	}
	
	private double laskeKokonaishinta(double aineen_hinta, double toimenpiteen_hinta, int pintojen_lkm) {
		return (aineen_hinta * pintojen_lkm + toimenpiteen_hinta);
	}
	
	private void paivitaPintakasittelyTiedot(Pintakasittelyaine valittuPintakasittely, Levitystapa valittuLevitystapa, 
			 TextField kokonaishintaTxt) {
		
		if (valittuLevitystapa != null) {
			
			kokonaishinta = Math.round(laskeKokonaishinta(getAineen_hinta(), getToimenpidehinta(), sivujen_lkm)*100.0)/100.0;
			
			if ( (!korkeusCheckBox.isSelected() && !leveysCheckBox.isSelected()) || (!korkeusCheckBox.isSelected() && !syvyysCheckBox.isSelected()) || 
					(!leveysCheckBox.isSelected() && !syvyysCheckBox.isSelected())) {
				kokonaishintaTxt.setText("Valitse mitat");
			}
			else {
				kokonaishintaTxt.setText(Double.toString(kokonaishinta));
			}
		}
		
	
	} // metodit laskemiseen yms
	Levitystapa levitystapaOlioResultSetista(ResultSet rs) throws SQLException {
			
		// Uusi olio Liitos malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Levitystapa levitystapaOlio = new Levitystapa();
		levitystapaOlio.setId(rs.getInt("id"));
		levitystapaOlio.setNimi(rs.getString("nimi"));
		levitystapaOlio.setPerusmaksu(rs.getDouble("perusmaksu"));
		levitystapaOlio.setAika(rs.getDouble("aika"));
		levitystapaOlio.setTuntihinta(rs.getDouble("tuntihinta"));
		
		return levitystapaOlio;	
	}
	
	// Getterit ja setterit
	private void setPintakasittelyLista() {
		
		try {
			haetutPintakasittelyaineet = pDao.haeKaikki();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Pintakasittelyaine x : haetutPintakasittelyaineet) {
			pkasittelyaineList.add(x);
		}
	}
	
	
	private void setLevitystapaLista() {
		
		try {
			haetutLevitystavat = levitysDao.haeKaikki();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		for (Levitystapa x : haetutLevitystavat) {
			levitystapaList.add(x);
		}

	
	}

	public int getLevitystapa_id() {
		return levitystapa_id;
	}

	public void setLevitystapa_id(int levitystapa_id) {
		this.levitystapa_id = levitystapa_id;
	}

	public double getPintaala() {
		return pintaala;
	}

	public void setPintaala(double pintaala) {
		this.pintaala = pintaala;
	}

	public double getToimenpidehinta() {
		return toimenpidehinta;
	}

	public void setToimenpidehinta(double toimenpidehinta) {
		this.toimenpidehinta = toimenpidehinta;
	}

	public double getKokonaishinta() {
		return kokonaishinta;
	}

	public void setKokonaishinta(double kokonaishinta) {
		this.kokonaishinta = kokonaishinta;
	}

	public double getAineen_kulutus() {
		return aineen_kulutus;
	}

	public void setAineen_kulutus(double aineen_kulutus) {
		this.aineen_kulutus = aineen_kulutus;
	}

	public double getAineen_hinta() {
		return aineen_hinta;
	}

	public void setAineen_hinta(double aineen_hinta) {
		this.aineen_hinta = aineen_hinta;
	}

	public Pintakasittelyaine getValittuPintakasittely() {
		return valittuPintakasittely;
	}

	public void setValittuPintakasittely(Pintakasittelyaine valittuPintakasittely) {
		this.valittuPintakasittely = valittuPintakasittely;
	}

	public Levitystapa getValittuLevitystapa() {
		return valittuLevitystapa;
	}

	public void setValittuLevitystapa(Levitystapa valittuLevitystapa) {
		this.valittuLevitystapa = valittuLevitystapa;
	}


	public int getValittu_id() {
		return valittu_id;
	}


	public void setValittu_id(int valittu_id) {
		this.valittu_id = valittu_id;
	}


	public String getValittu_nimi() {
		return valittu_nimi;
	}


	public void setValittu_nimi(String valittu_nimi) {
		this.valittu_nimi = valittu_nimi;
	}


	public double getValittu_leveys() {
		return valittu_leveys;
	}


	public void setValittu_leveys(double valittu_leveys) {
		this.valittu_leveys = valittu_leveys;
	}


	public double getValittu_korkeus() {
		return valittu_korkeus;
	}


	public void setValittu_korkeus(double valittu_korkeus) {
		this.valittu_korkeus = valittu_korkeus;
	}


	public double getValittu_syvyys() {
		return valittu_syvyys;
	}


	public void setValittu_syvyys(double valittu_syvyys) {
		this.valittu_syvyys = valittu_syvyys;
	}


	public double getValittu_hinta() {
		return valittu_hinta;
	}


	public void setValittu_hinta(double valittu_hinta) {
		this.valittu_hinta = valittu_hinta;
	}

	
	public CheckBox getSyvyysCheckBox() {
		return syvyysCheckBox;
	}



	public void setSyvyysCheckBox(CheckBox syvyysCheckBox) {
		this.syvyysCheckBox = syvyysCheckBox;
	}


	// Nappien kuuntelijat

	public LisaaKappaleController getTama() {
		return tama;
	}


	public void setTama(LisaaKappaleController tama) {
		this.tama = tama;
	}


	public Lisapalvelu getValittuLisapalvelu() {
		return valittuLisapalvelu;
	}


	public void setValittuLisapalvelu(Lisapalvelu valittuLisapalvelu) {
		this.valittuLisapalvelu = valittuLisapalvelu;
	}


	public boolean isHiotaanko() {
		return hiotaanko;
	}


	public void setHiotaanko(boolean hiotaanko) {
		this.hiotaanko = hiotaanko;
	}


	@FXML
	void peruutaPintakasittelynLisays(ActionEvent event) throws SQLException, IOException {
		
		// Loaderi
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaUusiKappale.fxml"));
		loader.load();
		
		FXMLLoader parentLoader = new FXMLLoader();
		parentLoader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
		// Kontrolleri luokka
		LisaaKappaleController controller = loader.getController();

		try {
			// Asetetaan Lisaa Kappale ikkunan elementit samoiksi kuin aikaisemmin ja disabloidaan uuden pintakasittelyn lisays
			// Nama valitettiin LisaaKappaleControllerin avaaLisaaPintakasittelyaineIkkuna -metodissa luokan settereilla.

			
			controller.getTable().getSelectionModel().select(getValittu_id());
			controller.getKappaleNimiTxt().setText(getValittu_nimi());
			controller.getLeveysTextBox().setText(Double.toString(getValittu_leveys()));
			controller.getKorkeusTextBox().setText(Double.toString(getValittu_korkeus()));
			controller.getSyvyysTextBox().setText(Double.toString(getValittu_syvyys()));
			controller.getKokonaisHinta().setText(Double.toString(getValittu_hinta()+kokonaishinta));
			

		} catch (NullPointerException e) {
		}	
		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Lisää uusi kappale");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.close();
	}

	@FXML
	void lisaaPintakasittely(ActionEvent event) throws SQLException, IOException{
	
		// Loaderi
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LisaaUusiKappale.fxml"));
		loader.load();
		
		FXMLLoader parentLoader = new FXMLLoader();
		parentLoader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
		
	//	parentLoader.setPintakasittelyKokonaishinta(kokonaishinta);
		
		// Kontrolleri luokka
		LisaaKappaleController controller = loader.getController();

		try {
			// Asetetaan Lisaa Kappale ikkunan elementit samoiksi kuin aikaisemmin ja disabloidaan uuden pintakasittelyn lisays
			// Nama valitettiin LisaaKappaleControllerin avaaLisaaPintakasittelyaineIkkuna -metodissa luokan settereilla.

			
			controller.getTable().getSelectionModel().select(getValittu_id());
			controller.getKappaleNimiTxt().setText(getValittu_nimi());
			controller.getLeveysTextBox().setText(Double.toString(getValittu_leveys()));
			controller.getKorkeusTextBox().setText(Double.toString(getValittu_korkeus()));
			controller.getSyvyysTextBox().setText(Double.toString(getValittu_syvyys()));
			controller.getKokonaisHinta().setText(Double.toString(getValittu_hinta()+kokonaishinta));
			controller.getLisaaPintakasittelyBtn().setDisable(true);
			if(hiotaanko) {
				controller.getHiontaCheckBox().setSelected(true);
			}
			else {
				controller.getHiontaCheckBox().setSelected(false);
			}
			controller.getLisamaksuComboBox().getSelectionModel().select(valittuLisapalvelu);
			
			// Valitetaan myos Lisaa kappale -ikkunalle tarvittavat uudet tasta ikkunasta saadut tiedot pintakasittelyista ja levitystavoista
			controller.setLevitystapaOlio(valittuLevitystapa);
			controller.setPkasittelyaineOlio(valittuPintakasittely);
			controller.getLevitystapaTxt().setText(valittuLevitystapa.getNimi());
			controller.getPkasittelyTxt().setText(valittuPintakasittely.getNimi());
			controller.setPintakasittelyKokonaishinta(kokonaishinta);
		} catch (NullPointerException e) {

		}
		
		if ( (!korkeusCheckBox.isSelected() && !leveysCheckBox.isSelected()) || (!korkeusCheckBox.isSelected() && !syvyysCheckBox.isSelected()) || 
				(!leveysCheckBox.isSelected() && !syvyysCheckBox.isSelected())) {
			System.out.println("Valitse vähintään kaksi mittaa!");
			return;
		}
		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Lisää uusi kappale");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.close();
			
	}
}
