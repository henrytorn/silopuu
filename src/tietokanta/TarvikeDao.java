package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Tarvike;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet helojen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class TarvikeDao implements Dao<Tarvike> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan Tarviken id
	 */
	@Override
	public Tarvike hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Tarvike haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Helat WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Tarvikea ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return TarvikeOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki Tarviket-taulun alkiot 
	 * 
	 * @return	lista kaikista Tarviket-taulun alkioista
	 */
	@Override
	public List<Tarvike> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Tarvike haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Helat";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon Tarvike-oliot tallennetaan
			List<Tarvike> Tarviket = new ArrayList<Tarvike>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi Tarvike-olio apumetodin TarvikeOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Tarvike TarvikeOlio = TarvikeOlioResultSetista(rs);
				Tarviket.add(TarvikeOlio);
			}
			
			// Palautetaan lista
			return Tarviket;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		

		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Tarvike-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	Tarvike-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Tarvike p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO Helat "
				+ "(nimi, kappalehinta, toimenpidehinta, toimenpideaika) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getToimenpidehinta());
			ps.setDouble(4, p.getToimenpideaika());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Tarvike p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE Helat SET "
				+ "nimi=?, kappalehinta=?, toimenpidehinta=?, toimenpideaika=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getToimenpidehinta());
			ps.setDouble(4, p.getToimenpideaika());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM Helat WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Tarvike-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Tarvike-luokan olioksi
	 * @return		Tarvike-luokan olio
	 * @throws SQLException
	 */
	public Tarvike TarvikeOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Tarvike malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Tarvike TarvikeOlio = new Tarvike();
		TarvikeOlio.setId(rs.getInt("id"));
		TarvikeOlio.setNimi(rs.getString("nimi"));
		TarvikeOlio.setKappalehinta(rs.getDouble("kappalehinta"));
		TarvikeOlio.setToimenpidehinta(rs.getDouble("toimenpidehinta"));
		TarvikeOlio.setToimenpideaika(rs.getDouble("toimenpideaika"));
		
		return TarvikeOlio;	
	}
	
}
