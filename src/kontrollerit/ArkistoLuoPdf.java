package kontrollerit;

import java.awt.Button;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import tietokanta.DBApurit;

public class ArkistoLuoPdf implements Initializable {

	@FXML public TextField nimis;
	@FXML public TextField leveysTextField;
	@FXML public TextField korkeusTextField;
	@FXML public TextField syvyysTextField;
	@FXML public TextField hintaTextField;
	@FXML public TextField asennusTextField;
	
	@FXML public TextArea kuvaus;
	@FXML public TextArea kuvaus1;
	@FXML public TextArea kuvaus2;
	@FXML public TextArea kuvaus3;
	@FXML public TextArea kuvaus4;
	
	String logoSijainti;
	
	PDFont font = PDType1Font.HELVETICA_BOLD;
	File directory = new File("Tiedostot");
	String tarvike;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
    
    	DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT logo FROM asetukset";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				
				logoSijainti = (rs.getString("logo"));
				
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}

	}
    
	
	@FXML
	void luoPdf(ActionEvent event) throws SQLException, IOException {
		
		  String tallennusNimi = nimis.getText();
		  
		  File f = new File(directory + "\\" + tallennusNimi + ".pdf");
		  if(f.exists()) { 
			  
			  Alert alert = new Alert(AlertType.ERROR);
		        alert.setTitle("Error?");
		        alert.setHeaderText("Nimell� " + tallennusNimi + " on jo tiedosto. Valitse uusi nimi.");
		        
				Optional<ButtonType> result = alert.showAndWait();
		        
		        }
		  else if(tallennusNimi.isEmpty()) {
			  
			  Alert alert = new Alert(AlertType.ERROR);
		        alert.setTitle("Error?");
		        alert.setHeaderText("Ei valittua nime�");
		        
				Optional<ButtonType> result = alert.showAndWait();
		        
		  }
		  else {
			  if (! directory.exists()){
		            directory.mkdir();
		        }

			  tuoteTiedot(tallennusNimi);		  
			  lisaaPerustiedot(tallennusNimi);
			  lisaaYritystiedot(tallennusNimi);
			  
			  Alert alert = new Alert(AlertType.INFORMATION);
		        alert.setTitle("Tallennettu");
		        alert.setHeaderText("Tallennettu");
		        
				Optional<ButtonType> result = alert.showAndWait();
		  }}
	      

	void tuoteTiedot(String tallennusNimi) throws IOException {
	
		
		PDFont font = PDType1Font.HELVETICA_BOLD;
		PDDocument document = new PDDocument();
	      
	      PDPage page = new PDPage(PDRectangle.A4);   
	      document.addPage(page);
	      PDPageContentStream contentStream = new PDPageContentStream(document, page);

	      //PDImageXObject logoImage = PDImageXObject.createFromFile("logo.png",document);
	      PDImageXObject logoImage = PDImageXObject.createFromFile(logoSijainti,document);
	      
	      contentStream.drawImage(logoImage, 20, 650);
	      
	      PDRectangle mediabox = page.getMediaBox();
        float margin = 120;
        float startY = mediabox.getUpperRightY() - margin;
        System.out.println(startY);
        float currentY = startY - 200;

        contentStream.beginText(); 
	     contentStream.setFont(font, 12);
	     contentStream.setLeading(16.5f);   
	      contentStream.newLineAtOffset(40, 650);
	      contentStream.showText(nimis.getText());
	      contentStream.newLine();
	      contentStream.newLine();
	      
	      String stext = kuvaus.getText().replaceAll("\n", "");
 
	      ArrayList<String> lines = new ArrayList<String>();
	      
	      float fontSize = 12;
	      float leading = 1.5f * fontSize;
	      float width = mediabox.getWidth() - 300;

	      int lastSpace = -1;

	      contentStream.newLineAtOffset(100, 0);

	      while (stext.length() > 0)
	      {
	          int spaceIndex = stext.indexOf(' ', lastSpace + 1);
	          if (spaceIndex < 0)
	              spaceIndex = stext.length();
	          String subString = stext.substring(0, spaceIndex);
	          float size = fontSize * font.getStringWidth(subString) / 1000;
	       
	          if (size > width)
	          {
	              if (lastSpace < 0)
	                  lastSpace = spaceIndex;
	              subString = stext.substring(0, lastSpace);
	              lines.add(subString);
	              stext = stext.substring(lastSpace).trim();
	            
	              lastSpace = -1;
	          }
	          else if (spaceIndex == stext.length())
	          {
	              lines.add(stext);
	              
	              stext = "";
	          }
	          else
	          {
	              lastSpace = spaceIndex;
	          }
	      }

	      for (String line: lines)
	      {
	    	  currentY -= 16.5f;

	    	  if(currentY<=margin)
	    	  { 
              	  contentStream.endText(); 
                  contentStream.close();
                  PDPage new_Page = new PDPage(PDRectangle.A4); 
                  document.addPage(new_Page);
                  contentStream = new PDPageContentStream(document, new_Page);
                  contentStream.beginText();
                  contentStream.setFont(font, 12);
                  contentStream.newLineAtOffset(140, 750);
                  contentStream.setLeading(16.5f);
                  currentY=startY;
              }
	          contentStream.showText(line);
	          contentStream.newLineAtOffset(0, -leading);
	      }
	      contentStream.showText("leveys: " + leveysTextField.getText());
    	  contentStream.newLine();
	      
    	  contentStream.showText("korkeus: " + korkeusTextField.getText());
    	  contentStream.newLine();
	      
    	  contentStream.showText("syvyys: " + syvyysTextField.getText());
	      contentStream.newLine();
	      contentStream.newLine();
	      
	      contentStream.newLineAtOffset(-100, 0);
	      lines.clear();
	      
	 
	      
	      
	      int i;
	      for(i = 0; i<6; i++){
	    	  
	    	  switch(i)
	    	  {
	    	
	    	  case 0: tarvike = "Helotus";
	    	  stext = kuvaus1.getText().replaceAll("\n", "");
	    	  
	    	  
	    	  
	    		  break;
	    	  case 1: tarvike = "Puulaji";
	    	  stext = kuvaus2.getText().replaceAll("\n", "");
			  	break;
	    	  case 2: tarvike = "Pintak�sittely";
	    	  stext = kuvaus3.getText().replaceAll("\n", "");
			  	break;
	    	  case 3: tarvike = "Muuta";
	    	  stext = kuvaus4.getText().replaceAll("\n", "");
			  	break;
	    	  case 4: tarvike = "Hinta-arvio";
	    	  stext = hintaTextField.getText().replaceAll("\n", "") + "�";
	    	  	break;
	    	  case 5: tarvike = "Asennus";
	    	  stext = asennusTextField.getText().replaceAll("\n", "");
	    	  	break;
  
	      }
	      
		contentStream.showText(tarvike);	      
		
	      contentStream.newLineAtOffset(100, 0);
	      
	      while (stext.length() > 0)
	      {
	          int spaceIndex = stext.indexOf(' ', lastSpace + 1);
	          if (spaceIndex < 0)
	              spaceIndex = stext.length();
	          String subString = stext.substring(0, spaceIndex);
	          float size = fontSize * font.getStringWidth(subString) / 1000;
	       
	          if (size > width)
	          {
	              if (lastSpace < 0)
	                  lastSpace = spaceIndex;
	              subString = stext.substring(0, lastSpace);
	              lines.add(subString);
	              stext = stext.substring(lastSpace).trim();
	            
	              lastSpace = -1;
	          }
	          else if (spaceIndex == stext.length())
	          {
	              lines.add(stext);
	              
	              stext = "";
	          }
	          else
	          {
	              lastSpace = spaceIndex;
	          }
	      }

	
	      for (String line: lines)
	      {
	    	  currentY -= 16.5f;

	    	  if(currentY<=margin)
	    	  { 
              	  contentStream.endText(); 
                  contentStream.close();
                  PDPage new_Page = new PDPage(PDRectangle.A4); 
                  document.addPage(new_Page);
                  contentStream = new PDPageContentStream(document, new_Page);
                  contentStream.beginText();
                  contentStream.setFont(font, 12);
                  contentStream.newLineAtOffset(140, 750);
                  contentStream.setLeading(16.5f);
                  currentY=startY;
              }
	          contentStream.showText(line);
	          contentStream.newLineAtOffset(0, -leading);

	      }
		  
	      contentStream.newLine();
	      
	      contentStream.newLineAtOffset(-100, 0);
	      lines.clear();

	      }
	      
	      contentStream.newLine();
   
	      contentStream.endText();
	      System.out.println("Tiedot lis�tty");
	      contentStream.close(); 
	      
	      
	      document.save(directory + "\\" + tallennusNimi + ".pdf");  
		  System.out.println("Tiedosto tallennettu");
		  document.close();
		  
	}
	void lisaaPerustiedot(String tallennusNimi) throws IOException {
		
		File pdfTiedosto = new File(directory + "\\" + tallennusNimi + ".pdf");
	    PDDocument document = PDDocument.load(pdfTiedosto);
	    int sivujenMaara = document.getNumberOfPages();
	    
	    for(int i=0; i<sivujenMaara; i++) {
	    	
	        PDPage page = document.getPage(i);
	        PDPageContentStream sivuNumerointi = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND,false,false);

	        sivuNumerointi.beginText();
	        sivuNumerointi.setFont(PDType1Font.HELVETICA_BOLD, 12);
	        sivuNumerointi.newLineAtOffset(525, 750);
	        sivuNumerointi.showText("( " + (i+1) + "/" + sivujenMaara + " )");
	        sivuNumerointi.endText();
	        sivuNumerointi.close();
	        
	        PDPageContentStream perusTiedot = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND,false,false);
	        
	        perusTiedot.beginText(); 
	        perusTiedot.setFont(PDType1Font.HELVETICA_BOLD, 12);
	        perusTiedot.setLeading(16.5f);
	        perusTiedot.newLineAtOffset(40, 750);
 
		    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy" );  
		    LocalDateTime now = LocalDateTime.now();  
		    System.out.println(dtf.format(now));  
		    
		    perusTiedot.newLineAtOffset(400, 0);
		    perusTiedot.showText("HINTA-ARVIO" );
		    perusTiedot.newLine();
		    perusTiedot.showText(dtf.format(now));
		    perusTiedot.newLine();
		    
		    String nimiss = nimis.getText().toUpperCase();
		    
		    ArrayList<String> lines = new ArrayList<String>();
		    
		    PDRectangle mediabox = page.getMediaBox();
		      float fontSize = 12;
		      float leading = 1.5f * fontSize;
		      float width = mediabox.getWidth() - 480;

		      int lastSpace = -1;
		      
		    while (nimiss.length() > 0)
		      {
		          int spaceIndex = nimiss.indexOf(' ', lastSpace + 1);
		          if (spaceIndex < 0)
		              spaceIndex = nimiss.length();
		          String subString = nimiss.substring(0, spaceIndex);
		          float size = fontSize * font.getStringWidth(subString) / 1000;
		       
		          if (size > width)
		          {
		              if (lastSpace < 0)
		                  lastSpace = spaceIndex;
		              subString = nimiss.substring(0, lastSpace);
		              lines.add(subString);
		              nimiss = nimiss.substring(lastSpace).trim();
		            
		              lastSpace = -1;
		          }
		          else if (spaceIndex == nimiss.length())
		          {
		              lines.add(nimiss);
		              
		              nimiss = "";
		          }
		          else
		          {
		              lastSpace = spaceIndex;
		          }
		      }

		
		      for (String line: lines)
		      {
		    	  perusTiedot.showText(line);
		    	  perusTiedot.newLineAtOffset(0, -leading);
		      }
		    
		    perusTiedot.endText();
		    perusTiedot.close();
	    }
	    document.save(directory + "\\" + tallennusNimi + ".pdf");
	    document.close();
	}
	
	void lisaaYritystiedot(String tallennusNimi) throws IOException {
		
	    File pdfTiedosto = new File(directory + "\\" + tallennusNimi + ".pdf");
	   
	    PDDocument document = PDDocument.load(pdfTiedosto);
	    	
	        PDPage page = document.getPage(0);
	        PDRectangle mediabox = page.getMediaBox();
	        
	        PDFont font = PDType1Font.HELVETICA_BOLD;
	      
	        String rivi = "Yritys";
	        String rivi2 = "Etu Suku, ammatti";
	        String rivi3 = "0504231245";
	        String rivi4 = "email@email.fi";
	        String rivi5 = "www.www.fi";
	        
	        int fontSize = 12;
	        
			float riviLeveys = font.getStringWidth(rivi)/1000*fontSize;
	        float startX = (mediabox.getWidth()-riviLeveys)/2 ;
	        
	        float riviLeveys2 = font.getStringWidth(rivi2)/1000*fontSize;
	        float startX2 = (mediabox.getWidth()-riviLeveys2)/2 ;
	        
	        float riviLeveys3 = font.getStringWidth(rivi3)/1000*fontSize;
	        float startX3 = (mediabox.getWidth()-riviLeveys3)/2 ;
	        
	        float riviLeveys4 = font.getStringWidth(rivi4)/1000*fontSize;
	        float startX4 = (mediabox.getWidth()-riviLeveys4)/2 ;
	        
	        float riviLeveys5 = font.getStringWidth(rivi5)/1000*fontSize;
	        float startX5 = (mediabox.getWidth()-riviLeveys5)/2 ;
	        
	
	        PDPageContentStream perusTiedotFooter = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND,false,false);

	        perusTiedotFooter.beginText();
	        perusTiedotFooter.setFont(font, 12);
	        perusTiedotFooter.newLineAtOffset(startX, 80);
	  
	        perusTiedotFooter.showText(rivi);
	        perusTiedotFooter.newLine();
	        perusTiedotFooter.newLineAtOffset(-startX + startX2, 0);
	        perusTiedotFooter.showText(rivi2);
	        perusTiedotFooter.newLine();
	        perusTiedotFooter.newLineAtOffset(-startX2 +startX3, 0);
	        perusTiedotFooter.showText(rivi3);
	        perusTiedotFooter.newLine();
	        perusTiedotFooter.newLineAtOffset(-startX3 +startX4, 0);
	        perusTiedotFooter.showText(rivi4);
	        perusTiedotFooter.newLine();
	        perusTiedotFooter.newLineAtOffset(-startX4 +startX5, 0);
	        perusTiedotFooter.showText(rivi5);
	        perusTiedotFooter.endText();
	        perusTiedotFooter.close();

	        document.save(directory + "\\" + tallennusNimi + ".pdf");
	    
	    document.close();
	}
	


	
	

}