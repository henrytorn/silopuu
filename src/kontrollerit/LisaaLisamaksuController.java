package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Lisapalvelu;
import tietokanta.LisapalveluDao;

public class LisaaLisamaksuController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField lisamaksuNimiTxt;
	
	@FXML
	private TextField lisamaksuHintaTxt;
	
	@FXML
	private TextField lisamaksuAsetteenTekeminenTxt;
	
	@FXML
	private TextField lisamaksuTuntihintaTxt;

	@FXML
	public Button tallennaLisamaksuBtn;
	
	@FXML
	public Button peruutaBtn;
	
	@FXML
	void peruutaLisamaksuLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLisamaksutIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaLisapalvelu(ActionEvent event) throws SQLException {
		
		// Uusi lisapalvelu-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Lisapalvelu lisapalveluOlio = new Lisapalvelu();
		lisapalveluOlio.setId(idGenerator());
		lisapalveluOlio.setNimi(lisamaksuNimiTxt.getText());
		lisapalveluOlio.setHinta(Double.parseDouble(lisamaksuHintaTxt.getText()));
		lisapalveluOlio.setAsetteen_tekeminen(Double.parseDouble(lisamaksuAsetteenTekeminenTxt.getText()));
		lisapalveluOlio.setTuntihinta(Double.parseDouble(lisamaksuTuntihintaTxt.getText()));
		
		// LisapalveluDao-luokan metodilla lisays tietokantaan
		LisapalveluDao p = new LisapalveluDao();
		if (p.lisaa(lisapalveluOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaLisamaksutIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
