package mallit;

public class Tarvike {
	
	private int id;
	private String nimi;
	private double kappalehinta;
	private double toimenpidehinta;
	private double toimenpideaika;
	
	public Tarvike() {
	}
	
	public Tarvike(int id, String nimi, double kappalehinta, double toimenpidehinta, double toimenpideaika) {
		this.id = id;
		this.nimi = nimi;
		this.kappalehinta = kappalehinta;
		this.toimenpidehinta = toimenpidehinta;
		this.toimenpideaika = toimenpideaika;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getKappalehinta() {
		return kappalehinta;
	}

	public void setKappalehinta(double kappalehinta) {
		this.kappalehinta = kappalehinta;
	}

	public double getToimenpidehinta() {
		return toimenpidehinta;
	}

	public void setToimenpidehinta(double toimenpidehinta) {
		this.toimenpidehinta = toimenpidehinta;
	}

	public double getToimenpideaika() {
		return toimenpideaika;
	}

	public void setToimenpideaika(double toimenpideaika) {
		this.toimenpideaika = toimenpideaika;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Hinta: " + this.kappalehinta + ", Toimenpideaika: " + this.toimenpideaika;
	}
}
