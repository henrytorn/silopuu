package mallit;

public class Tiedosto {
	
	private String nimi;
	private String luomisaika;

	
	public Tiedosto() {
	}
	
	public Tiedosto(String nimi, String luomisaika ) {
		
		this.nimi = nimi;
		this.setLuomisaika(luomisaika);
		
	}
	
	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	
	public String getLuomisaika() {
		return luomisaika;
	}

	public void setLuomisaika(String luomisaika) {
		this.luomisaika = luomisaika;
	}
	

	@Override
	public String toString() {
		return "Nimi:: " + this.nimi + ", Luomisaika: " + this.luomisaika ;
	}

	
}
