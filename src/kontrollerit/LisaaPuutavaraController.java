package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Puutavara;
import tietokanta.PuutavaraDao;

public class LisaaPuutavaraController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField PuutavaraNimiTxt;
	
	@FXML
	private TextField PuutavaraHintaTxt;
	
	@FXML
	private TextField PuutavaraNelioHintaTxt;
	
	@FXML
	private TextField PuutavaraMassaTxt;
	
	@FXML
	public Button tallennaPuutavaraBtn;
	
	@FXML
	public Button peruutaBtn;
	
	@FXML
	public TietokantaController TC ;
	
	@FXML
	void peruutaPuutavaraLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaPuutavaratIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}


	@FXML
	void lisaaPuutavara(ActionEvent event) throws SQLException {
		
		// Uusi Puutavara-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Puutavara PuutavaraOlio = new Puutavara();
		PuutavaraOlio.setId(idGenerator());
		PuutavaraOlio.setNimi(PuutavaraNimiTxt.getText());
		PuutavaraOlio.setKuutiohinta(Double.parseDouble(PuutavaraHintaTxt.getText()));
		PuutavaraOlio.setNeliohinta(Double.parseDouble(PuutavaraNelioHintaTxt.getText()));
		PuutavaraOlio.setTiheys(Double.parseDouble(PuutavaraMassaTxt.getText()));
		
		// PuutavaraDao-luokan metodilla lisays tietokantaan
		PuutavaraDao p = new PuutavaraDao();
		if (p.lisaa(PuutavaraOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaPuutavaratIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
		
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
