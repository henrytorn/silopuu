package mallit;

public class ArkistoTuote {
	
	private int id;
	private String nimi;
	private double leveys, korkeus, syvyys, hinta;
	
	public ArkistoTuote() {
	}
	
	public ArkistoTuote(int id, String nimi, double hinta) {
		this.id = id;
		this.nimi = nimi;
		this.hinta = hinta;
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

//	public double getLeveys() {
//		return leveys;
//	}
//
//	public void setLeveys(double leveys) {
//		this.leveys = leveys;
//	}
//
//	public double getKorkeus() {
//		return korkeus;
//	}
//
//	public void setKorkeus(double korkeus) {
//		this.korkeus = korkeus;
//	}
//
//	public double getSyvyys() {
//		return syvyys;
//	}
//
//	public void setSyvyys(double syvyys) {
//		this.syvyys = syvyys;
//	}

	public double getHinta() {
		return hinta;
	}

	public void setHinta(double hinta) {
		this.hinta = hinta;
	}

	@Override
	public String toString() {
		return "this.nimi";
		//TODO
	}


}
