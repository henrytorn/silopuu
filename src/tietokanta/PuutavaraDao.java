package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Puutavara;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet Puutavaraen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class PuutavaraDao implements Dao<Puutavara> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan Puutavaran id
	 */
	@Override
	public Puutavara hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Puutavara haetaan annetulla parametrilla "id"
		String sql = "SELECT id, nimi, kuutiohinta, tiheys FROM Puulajit WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Puutavaraa ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return PuutavaraOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki Puutavarat-taulun alkiot 
	 * 
	 * @return	lista kaikista Puutavarat-taulun alkioista
	 */
	@Override
	public List<Puutavara> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu Puutavara haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Puulajit";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon Puutavara-oliot tallennetaan
			List<Puutavara> Puutavarat = new ArrayList<Puutavara>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi Puutavara-olio apumetodin PuutavaraOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Puutavara PuutavaraOlio = PuutavaraOlioResultSetista(rs);
				Puutavarat.add(PuutavaraOlio);
			}
			
			// Palautetaan lista
			return Puutavarat;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Puutavara-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	Puutavara-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Puutavara p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO Puulajit "
				+ "(nimi, kuutiohinta,neliohinta, tiheys) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKuutiohinta());
			ps.setDouble(3, p.getNeliohinta());
			ps.setDouble(4, p.getTiheys());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Puutavara p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE Puulajit SET "
				+ "nimi=?, kuutiohinta=?, neliohinta=? ,tiheys=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKuutiohinta());
			ps.setDouble(3, p.getNeliohinta());
			ps.setDouble(4, p.getTiheys());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM Puulajit WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Puutavara-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Puutavara-luokan olioksi
	 * @return		Puutavara-luokan olio
	 * @throws SQLException
	 */
	public Puutavara PuutavaraOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Puutavara malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Puutavara PuutavaraOlio = new Puutavara();
		PuutavaraOlio.setId(rs.getInt("id"));
		PuutavaraOlio.setNimi(rs.getString("nimi"));
		PuutavaraOlio.setKuutiohinta(rs.getDouble("kuutiohinta"));
		PuutavaraOlio.setNeliohinta(rs.getDouble("neliohinta"));
		PuutavaraOlio.setTiheys(rs.getDouble("tiheys"));
		
		return PuutavaraOlio;	
	}
	
}
