package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.ArkistoKappale;
import mallit.ArkistoKappaleTauluun;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet ArkistoKappaleen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class ArkistoKappaleTauluunDao implements Dao<ArkistoKappaleTauluun> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan ArkistoKappalen id
	 */
	@Override
	public ArkistoKappaleTauluun hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM arkisto_kappaleet WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return ArkistoKappaleTauluunOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki ArkistoKappalet-taulun alkiot 
	 * 
	 * @return	lista kaikista ArkistoKappalet-taulun alkioista
	 */
	@Override
	public List<ArkistoKappaleTauluun> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu ArkistoKappale haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM arkisto_kappaleet";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon ArkistoKappale-oliot tallennetaan
			List<ArkistoKappaleTauluun> ArkistoKappaleet = new ArrayList<ArkistoKappaleTauluun>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi ArkistoKappale-olio apumetodin ArkistoKappaleTauluunOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				ArkistoKappaleTauluun ArkistoKappaleTauluunOlio = ArkistoKappaleTauluunOlioResultSetista(rs);
				ArkistoKappaleet.add(ArkistoKappaleTauluunOlio);
			}
			
			// Palautetaan lista
			return ArkistoKappaleet;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun ArkistoKappale-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	ArkistoKappale-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(ArkistoKappaleTauluun p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO arkisto_kappaleet "
				+ "(nimi, leveys, korkeus, syvyys, hinta, pintakasittely_id, puutavara_id, levitystapa_id, lisamaksu_id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLeveys());
			ps.setDouble(3, p.getKorkeus());
			ps.setDouble(4, p.getSyvyys());
			ps.setDouble(5, p.getHinta());
			ps.setInt(6, p.getPintakasittely_id());
			ps.setInt(7, p.getPuutavara_id());
			ps.setInt(8, p.getLevitystapa_id());
			ps.setInt(9, p.getLisamaksu_id());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(ArkistoKappaleTauluun p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE arkisto_kappaleet SET "
				+ "nimi=?, leveys=?, korkeus=?, syvyys=?, hinta=?, pintakasittely_id=?, puutavara_id=?, levitystapa_id=?, lisamaksu_id=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLeveys());
			ps.setDouble(3, p.getKorkeus());
			ps.setDouble(4, p.getSyvyys());
			ps.setDouble(5, p.getHinta());
			ps.setInt(6, p.getPintakasittely_id());
			ps.setInt(7, p.getPuutavara_id());
			ps.setInt(8, p.getLevitystapa_id());
			ps.setInt(9, p.getLisamaksu_id());
			ps.setInt(10, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM arkisto_kappaleet WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * ArkistoKappale-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota ArkistoKappale-luokan olioksi
	 * @return		ArkistoKappale-luokan olio
	 * @throws SQLException
	 */
	
	// TODO NOTE TO SELF (MIIKKA): tee tasta modulaarisempaa ja siistimpaa, kamalan nakoista nyt
	public ArkistoKappaleTauluun ArkistoKappaleTauluunOlioResultSetista(ResultSet rs) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		
		String pintakasittely_nimi = null, levitystapa_nimi = null, tarvike_nimi = null, puutavara_nimi = null, lisamaksu_nimi = null, liitos_nimi = null;
		
		int id = rs.getInt("pintakasittely_id");
		String sql = "SELECT nimi FROM pintakasittelyaineet WHERE id = ?";

		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs1 = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs1 = ps.executeQuery();
			
			if (rs1 == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		if (rs1.next() == true) {
			pintakasittely_nimi = rs1.getString("nimi");
		}
		
		id = rs.getInt("puutavara_id");
		sql = "SELECT nimi FROM puulajit WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		rs1 = null;
		ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs1 = ps.executeQuery();
			
			if (rs1 == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}

		if (rs1.next() == true) {
			puutavara_nimi = rs1.getString("nimi");
		}
		
		
		id = rs.getInt("levitystapa_id");
		sql = "SELECT nimi FROM levitystapa WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		rs1 = null;
		ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs1 = ps.executeQuery();
			
			if (rs1 == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}

		if (rs1.next() == true) {
			levitystapa_nimi = rs1.getString("nimi");
		}
		
		
		id = rs.getInt("lisamaksu_id");
		sql = "SELECT nimi FROM lisapalvelut WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		rs1 = null;
		ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs1 = ps.executeQuery();
			
			if (rs1 == null) {
				System.out.println("ArkistoKappaletta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}

		if (rs1.next() == true) {
			lisamaksu_nimi = rs1.getString("nimi");
		}
		
		// Uusi olio ArkistoKappale malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		ArkistoKappaleTauluun ArkistoKappaleTauluunOlio = new ArkistoKappaleTauluun();
		ArkistoKappaleTauluunOlio.setId(rs.getInt("id"));
		ArkistoKappaleTauluunOlio.setNimi(rs.getString("nimi"));
		ArkistoKappaleTauluunOlio.setHinta(rs.getDouble("hinta"));
		ArkistoKappaleTauluunOlio.setLeveys(rs.getDouble("leveys"));
		ArkistoKappaleTauluunOlio.setKorkeus(rs.getDouble("korkeus"));
		ArkistoKappaleTauluunOlio.setSyvyys(rs.getDouble("syvyys"));
		ArkistoKappaleTauluunOlio.setPintakasittely_id(rs.getInt("pintakasittely_id"));
		ArkistoKappaleTauluunOlio.setPuutavara_id(rs.getInt("puutavara_id"));
		ArkistoKappaleTauluunOlio.setLevitystapa_id(rs.getInt("levitystapa_id"));
		ArkistoKappaleTauluunOlio.setLisamaksu_id(rs.getInt("lisamaksu_id"));
		ArkistoKappaleTauluunOlio.setPintakasittely(pintakasittely_nimi);
		ArkistoKappaleTauluunOlio.setLisamaksu(lisamaksu_nimi);
		ArkistoKappaleTauluunOlio.setLevitystapa(levitystapa_nimi);
		ArkistoKappaleTauluunOlio.setPuutavara(puutavara_nimi);
		
		return ArkistoKappaleTauluunOlio;	
	}
	
}
