package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Liitos;
import mallit.Puutavara;
import tietokanta.DBApurit;
import tietokanta.LiitosDao;
import kontrollerit.Apumetodeja;

public class LiitosViewController implements Initializable{

	@FXML private TableView<Liitos> table;
	@FXML private TableColumn<Liitos, Integer> id;
	@FXML private TableColumn<Liitos, String> nimi;
	@FXML private TableColumn<Liitos, Double> kappalehinta;
	@FXML private TableColumn<Liitos, Double> tuntihinta;
	@FXML private TableColumn<Liitos, Double> asetteen_tekeminen;
	@FXML public Button lisaaLiitosBtn;
	
	ObservableList<Liitos> liitoslist = FXCollections.observableArrayList();
	
	@FXML
	public TextField hakuTBox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Liitokset";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				liitoslist.add(new Liitos(rs.getInt("id"),rs.getString("nimi"),Apumetodeja.round(rs.getDouble("kappalehinta"), 2),Apumetodeja.round(rs.getDouble("tuntihinta"), 2), 
						rs.getDouble("asetteen_tekeminen") ));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		
		id.setCellValueFactory(new PropertyValueFactory<Liitos, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Liitos, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("kappalehinta"));
		tuntihinta.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("tuntihinta"));
		asetteen_tekeminen.setCellValueFactory(new PropertyValueFactory<Liitos, Double>("asetteen_tekeminen"));

		table.setItems(liitoslist);
		
		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
			try {
				haeLiitokset(conn, newText);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	void avaaMuokkaaLiitosIkkuna(ActionEvent event) throws SQLException, IOException {
		
		// Ladataan loaderiin fxml tietoja
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/MuokkaaLiitos.fxml"));
		loader.load();
		
		// Kontrollerin alustus ja olion valinta tableviewista
		MuokkaaLiitosController controller = loader.getController();
    	Liitos valittuLiitos = table.getSelectionModel().getSelectedItem();

    	// Jos valittiin tietue taulusta, valitetaan uuden ikkunan kontrollerille tiedot gettereilla ja settereilla,
    	// avataan uusi ikkuna ja suljetaan vanha
		if (valittuLiitos != null) {
			controller.setValittuLiitos(valittuLiitos);
			controller.getLiitosHintaTxt().setText(Double.toString(valittuLiitos.getKappalehinta()));
			controller.getLiitosTuntihintaTxt().setText(Double.toString(valittuLiitos.getTuntihinta()));
			controller.getLiitosNimiTxt().setText(valittuLiitos.getNimi());
			
			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Muokkaa");
			stage.setScene(new Scene(root, 365, 410));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		}
	}
	
	@FXML
	void avaaLisaaLiitosIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/LisaaLiitos.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisää liitos");
    		stage.setScene(new Scene(root, 365, 410));
    		stage.show();
    		
    		Stage stages = (Stage) lisaaLiitosBtn.getScene().getWindow();
    		stages.close();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void poistaLiitos(ActionEvent event) throws SQLException {

    	Liitos valittuLiitos = table.getSelectionModel().getSelectedItem();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Oletko varma?");
    	alert.setHeaderText("Haluatko varmasti poistaa liitoksen " + valittuLiitos.getNimi() + "?");
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK) { 
    	
    		if (valittuLiitos != null) {
    			try {
    				LiitosDao dao = new LiitosDao();
    				if (dao.poista(valittuLiitos.getId())) {
    					System.out.println("Poistettiin liitos: " + valittuLiitos.toString());
    				}
    				else {
    					System.out.println("Poisto epaonnistui");
    					//TODO parempi ilmoitus
    				}
    			} catch (SQLException se) {
    				se.printStackTrace();
    			}
	
    			liitoslist.removeAll(liitoslist);
    			initialize(null, null);
    		}
    	}
		
	}
	
	@FXML 
	void takaisin(ActionEvent event) throws SQLException {

		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	@FXML
	void haeLiitokset(Connection conn, String text) throws SQLException {
		String sql = "SELECT * FROM Liitokset WHERE nimi LIKE '%" + text + "%'";

		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			liitoslist.clear();

			while (rs.next()) {
				liitoslist.add(new Liitos(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("tuntihinta"), rs.getDouble("asetteen_tekeminen")));
			}

		} catch (SQLException se) {
			se.printStackTrace();
		}

	}
	
}