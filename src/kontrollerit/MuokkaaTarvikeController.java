package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Tarvike;
import tietokanta.TarvikeDao;

public class MuokkaaTarvikeController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField TarvikeNimiTxt;
	
	@FXML
	private TextField TarvikeHintaTxt;
	
	@FXML
	private TextField TarvikeToimenpideaikaTxt;
	
	@FXML
	private TextField TarvikeToimenpidehintaTxt;
	
	@FXML
	private Button tallennaTarvikeBtn;
	
	@FXML
	private Button peruutaBtn;
	
	private Tarvike valittuTarvike;
	
	@FXML
	void peruutaTarvikeMuokkaus(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaTarviketIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	
	}
	
	@FXML
	void muokkaaTarvike(ActionEvent event) throws SQLException {
		
		Tarvike TarvikeOlio = new Tarvike();
		System.out.println(valittuTarvike.toString());
		// Tassa manuaalisesti laitettu id testitarkoitukseen, silla
		// kayttoliittymassa ei ole mahdollisuutta viela valita haluttua
		// tietuetta eli id:ta
		TarvikeOlio.setNimi(TarvikeNimiTxt.getText());
		TarvikeOlio.setKappalehinta(Double.parseDouble(TarvikeHintaTxt.getText()));
		TarvikeOlio.setToimenpideaika(Double.parseDouble(TarvikeToimenpideaikaTxt.getText()));
		TarvikeOlio.setToimenpidehinta(Double.parseDouble(TarvikeToimenpidehintaTxt.getText()));
		TarvikeOlio.setId(valittuTarvike.getId());
		System.out.println(TarvikeOlio.toString());
		
		TarvikeDao p = new TarvikeDao();
		if (p.paivita(TarvikeOlio)) {
			System.out.println("Muokkaus onnistui");
		}
		else {
			System.out.println("Muokkaus epäonnistui");
		}
		
		TietokantaController tc = new TietokantaController();
		tc.avaaTarviketIkkuna(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}

	public TextField getTarvikeNimiTxt() {
		return TarvikeNimiTxt;
	}

	public void setTarvikeNimiTxt(TextField TarvikeNimiTxt) {
		this.TarvikeNimiTxt = TarvikeNimiTxt;
	}

	public TextField getTarvikeHintaTxt() {
		return TarvikeHintaTxt;
	}

	public void setTarvikeHintaTxt(TextField TarvikeHintaTxt) {
		this.TarvikeHintaTxt = TarvikeHintaTxt;
	}

	public TextField getTarvikeToimenpideaikaTxt() {
		return TarvikeToimenpideaikaTxt;
	}

	public void setTarvikeToimenpideaikaTxt(TextField TarvikeToimenpideaikaTxt) {
		this.TarvikeToimenpideaikaTxt = TarvikeToimenpideaikaTxt;
	}

	public TextField getTarvikeToimenpidehintaTxt() {
		return TarvikeToimenpidehintaTxt;
	}

	public void setTarvikeToimenpidehintaTxt(TextField TarvikeToimenpidehintaTxt) {
		this.TarvikeToimenpidehintaTxt = TarvikeToimenpidehintaTxt;
	}

	public Tarvike getValittuTarvike() {
		return valittuTarvike;
	}

	public void setValittuTarvike(Tarvike valittuTarvike) {
		this.valittuTarvike = valittuTarvike;
	}
	
}
