package application;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Aloitussivu extends Application {
	public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Aloitussivu.fxml"));
        primaryStage.setTitle("Aloitussivu");
        primaryStage.setScene(new Scene(root, 415, 365));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}