package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Pintakasittelyaine;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet helojen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class PintakasittelyaineDao implements Dao<Pintakasittelyaine> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan pintakasittelyaineen id
	 */
	@Override
	public Pintakasittelyaine hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu pintakasittelyaine haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Pintakasittelyaineet WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Pintakasittelyainetta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return pintakasittelyaineOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki pintakasittelyaineet-taulun alkiot 
	 * 
	 * @return	lista kaikista pintakasittelyaineet-taulun alkioista
	 */
	@Override
	public List<Pintakasittelyaine> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu pintakasittelyaine haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Pintakasittelyaineet";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon pintakasittelyaine-oliot tallennetaan
			List<Pintakasittelyaine> pintakasittelyaineet = new ArrayList<Pintakasittelyaine>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi pintakasittelyaine-olio apumetodin pintakasittelyaineOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Pintakasittelyaine pintakasittelyaineOlio = pintakasittelyaineOlioResultSetista(rs);
				pintakasittelyaineet.add(pintakasittelyaineOlio);
			}
			
			// Palautetaan lista
			return pintakasittelyaineet;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		

		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Pintakasittelyaine-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	pintakasittelyaine-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Pintakasittelyaine p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO pintakasittelyaineet "
				+ "(nimi, litrahinta, riittoisuus) VALUES (?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLitrahinta());
			ps.setDouble(3, p.getRiittoisuus());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Pintakasittelyaine p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE pintakasittelyaineet SET "
				+ "nimi=?, litrahinta=?, riittoisuus=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getLitrahinta());
			ps.setDouble(3, p.getRiittoisuus());
			ps.setDouble(4, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM pintakasittelyaineet WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Pintakasittelyaine-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Pintakasittelyaine-luokan olioksi
	 * @return		Pintakasittelyaine-luokan olio
	 * @throws SQLException
	 */
	public Pintakasittelyaine pintakasittelyaineOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Pintakasittelyaine malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Pintakasittelyaine pintakasittelyaineOlio = new Pintakasittelyaine();
		pintakasittelyaineOlio.setId(rs.getInt("id"));
		pintakasittelyaineOlio.setNimi(rs.getString("nimi"));
		pintakasittelyaineOlio.setLitrahinta(rs.getDouble("litrahinta"));
		pintakasittelyaineOlio.setRiittoisuus(rs.getDouble("riittoisuus"));
		
		return pintakasittelyaineOlio;	
	}
	
}
