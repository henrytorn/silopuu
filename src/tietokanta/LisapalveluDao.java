package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import kontrollerit.Apumetodeja;

import mallit.Lisapalvelu;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet helojen lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class LisapalveluDao implements Dao<Lisapalvelu> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan lisapalvelun id
	 */
	@Override
	public Lisapalvelu hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu lisapalvelu haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Lisapalvelut WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Lisapalvelua ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return lisapalveluOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki lisapalvelut-taulun alkiot 
	 * 
	 * @return	lista kaikista lisapalvelut-taulun alkioista
	 */
	@Override
	public List<Lisapalvelu> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu lisapalvelu haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Lisapalvelut";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon lisapalvelu-oliot tallennetaan
			List<Lisapalvelu> lisapalvelut = new ArrayList<Lisapalvelu>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi lisapalvelu-olio apumetodin lisapalveluOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Lisapalvelu lisapalveluOlio = lisapalveluOlioResultSetista(rs);
				lisapalvelut.add(lisapalveluOlio);
			}
			
			// Palautetaan lista
			return lisapalvelut;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		

		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Lisapalvelu-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	lisapalvelu-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Lisapalvelu p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO lisapalvelut "
				+ "(nimi, hinta, tuntihinta, asetteen_tekeminen) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getHinta());
			ps.setDouble(3, p.getTuntihinta());
			ps.setDouble(4, p.getAsetteen_tekeminen());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Lisapalvelu p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE lisapalvelut SET "
				+ "nimi=?, hinta=?, tuntihinta=?, asetteen_tekeminen=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getHinta());
			ps.setDouble(3, p.getTuntihinta());
			ps.setDouble(4, p.getAsetteen_tekeminen());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM lisapalvelut WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Lisapalvelu-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Lisapalvelu-luokan olioksi
	 * @return		Lisapalvelu-luokan olio
	 * @throws SQLException
	 */
	public Lisapalvelu lisapalveluOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Lisapalvelu malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Lisapalvelu lisapalveluOlio = new Lisapalvelu();
		lisapalveluOlio.setId(rs.getInt("id"));
		lisapalveluOlio.setNimi(rs.getString("nimi"));
		lisapalveluOlio.setHinta(Apumetodeja.round(rs.getDouble("hinta"), 2));
		lisapalveluOlio.setAsetteen_tekeminen(rs.getDouble("asetteen_tekeminen"));
		lisapalveluOlio.setTuntihinta(Apumetodeja.round(rs.getDouble("tuntihinta"), 2));
		
		return lisapalveluOlio;	
	}
	
}
