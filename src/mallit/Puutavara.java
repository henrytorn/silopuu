package mallit;

public class Puutavara {
	
	private int id;
	private String nimi;
	private double kuutiohinta;
	private double neliohinta;
	private double tiheys;
	
	public Puutavara() {
	}
	
	public Puutavara(int id, String nimi, double kuutiohinta, double neliohinta, double tiheys) {
		this.id = id;
		this.nimi = nimi;
		this.kuutiohinta = kuutiohinta;
		this.neliohinta = neliohinta;
		this.tiheys = tiheys;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getKuutiohinta() {
		return kuutiohinta;
	}

	public void setKuutiohinta(double kuutiohinta) {
		this.kuutiohinta = kuutiohinta;
	}

	public double getTiheys() {
		return tiheys;
	}

	public void setTiheys(double tiheys) {
		this.tiheys = tiheys;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Kuutiohinta: " + this.kuutiohinta + ", Neliöhinta: " + this.neliohinta + ", " + ", Tiheys: "  + this.tiheys;
	}

	public double getNeliohinta() {
		return neliohinta;
	}

	public void setNeliohinta(double neliohinta) {
		this.neliohinta = neliohinta;
	}
}
