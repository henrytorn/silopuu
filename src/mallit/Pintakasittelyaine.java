package mallit;

public class Pintakasittelyaine {
	
	private int id;
	private String nimi;
	private double litrahinta;
	private double riittoisuus;
	
	public Pintakasittelyaine() {
	}
	
	public Pintakasittelyaine(int id, String nimi, double litrahinta, double riittoisuus) {
		this.id = id;
		this.nimi = nimi;
		this.litrahinta = litrahinta;
		this.riittoisuus = riittoisuus;
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getLitrahinta() {
		return litrahinta;
	}

	public void setLitrahinta(double litrahinta) {
		this.litrahinta = litrahinta;
	}


	public double getRiittoisuus() {
		return riittoisuus;
	}

	public void setRiittoisuus(double riittoisuus) {
		this.riittoisuus = riittoisuus;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Hinta: " + this.litrahinta + ", Riittoisuus: " + this.riittoisuus;
	}
}
