package kontrollerit;
import java.sql.SQLException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mallit.Liitos;
import tietokanta.LiitosDao;

public class LisaaLiitosController {
	
	// Kayttoliittyman kytkennat
	@FXML
	private TextField liitosNimiTxt;
	
	@FXML
	private TextField liitosHintaTxt;
	
	@FXML
	private TextField liitosTuntihintaTxt;
	
	@FXML
	public Button tallennaLiitosBtn;
	
	@FXML
	public Button peruutaBtn;
	
	@FXML
	private TextField liitosAsetteenTekeminenTxt;
	
	@FXML
	void peruutaLisays(ActionEvent event) throws SQLException {
		
		//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
		TietokantaController tc = new TietokantaController();
		tc.avaaLiitosIkkuna(event);
		
		//LIS�YSIKKUNAN POISTO
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
			
	
	}
	
	@FXML
	void lisaaLiitos(ActionEvent event) throws SQLException {
		
		// Uusi liitos-olio ja siihen tiedot kayttoliittyman TextFieldeista
		Liitos liitosOlio = new Liitos();
		liitosOlio.setId(idGenerator());
		liitosOlio.setNimi(liitosNimiTxt.getText());
		liitosOlio.setKappalehinta(Double.parseDouble(liitosHintaTxt.getText()));
		liitosOlio.setTuntihinta(Double.parseDouble(liitosTuntihintaTxt.getText()));
		liitosOlio.setAsetteen_tekeminen(Double.parseDouble(liitosAsetteenTekeminenTxt.getText()));
		
		// LiitosDao-luokan metodilla lisays tietokantaan
		LiitosDao p = new LiitosDao();
		if (p.lisaa(liitosOlio)) {
			System.out.println("Lisäys onnistui");
			// TODO, parempi ilmoitus
			
			//IKKUNAN VAIHTO // TIETOKANTACONTROLLERILTA
			TietokantaController tc = new TietokantaController();
			tc.avaaLiitosIkkuna(event);
			
			//LIS�YSIKKUNAN POISTO
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			
		}
		else {
			System.out.println("Lisäys epäonnistui");
			// TODO, parempi ilmoitus
		}
	}
	
	
	// Valiaikainen apumetodi, jolla saadaan satunnainen luku valilta 1-1000 id:ta varten
	// Tulee varmaan heittamaan unique constraint erroria tjsp jos sattuu napsahtamaan jo
	// tietokannassa oleva id
	public int idGenerator() {
		Random r = new Random();
		int low = 10;
		int high = 1000;
		return r.nextInt(high-low) + low;
	}
}
