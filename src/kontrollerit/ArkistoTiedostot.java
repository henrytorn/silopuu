package kontrollerit;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javafx.application.HostServices;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Puutavara;
import mallit.Tiedosto;
import tietokanta.PuutavaraDao;

public class ArkistoTiedostot implements Initializable{

	@FXML 
	private TableView<Tiedosto> table;
	
	@FXML 
	private TableColumn<Tiedosto, String> nimi;
	
	@FXML 
	private TableColumn<Tiedosto, String> luomisaika;
	
	ObservableList<Tiedosto> details = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		File rootFolder = new File("tiedostot" + "\\");
		File[] filesList = rootFolder.listFiles();

		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		
        for(File f : filesList){
        	
        	try {
        		
        	Path path = Paths.get("tiedostot" + "\\" + f.getName());
        	
    	    BasicFileAttributes attr;
    	    attr = Files.readAttributes(path, BasicFileAttributes.class);
    	     
    	    if(f.isFile()){

                System.out.println(f.getName());
                System.out.println(format.format(new Date(attr.creationTime().toMillis())));

                details.add(new Tiedosto(f.getName().toString(), format.format(new Date(attr.creationTime().toMillis()))));
                
        	    nimi.setCellValueFactory(new PropertyValueFactory<Tiedosto, String>("nimi"));
        	    luomisaika.setCellValueFactory(new PropertyValueFactory<Tiedosto, String>("luomisaika"));
            }
    	    
    	    } catch (IOException e) {
    	    System.out.println("oops error! " + e.getMessage());
    	    }
        }
       
        	table.setItems(details);
        
        	System.out.println(details);
    
	}
	@FXML
	void deleteSelectedFile(ActionEvent event) throws SQLException {
		
		Tiedosto valittuTiedosto = table.getSelectionModel().getSelectedItem();
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Oletko varma?");
        alert.setHeaderText("Haluatko varmasti poistaa " + valittuTiedosto.getNimi() + " tiedoston?");
        
		Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){

        if (valittuTiedosto != null) {

		try{
		File file = new File("Tiedostot" + "\\" + valittuTiedosto.getNimi());
		
		if(file.delete()) {
			System.out.println(file.getName() + " On poistettu");
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
			
			Parent root;
	    	try {
	    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/ArkistoTiedostot.fxml"));
	    		Stage stage = new Stage();
	    		stage.setTitle("Tiedostot");
	    		stage.setScene(new Scene(root, 1280, 768));
	    		stage.show();
	    	
	    	}
	    	catch (IOException e) {
	    		e.printStackTrace();
	    	}	
		
		}
			
		else{
			System.out.println("Poisto ei onnistunut.");
		}
	}catch(Exception e){
		
		e.printStackTrace();
		
	}}}
	
}

	@FXML
	void openSelectedFile(ActionEvent event) throws SQLException {

		Tiedosto valittuTiedosto = table.getSelectionModel().getSelectedItem();
		
		 System.out.println("Valittu: " + valittuTiedosto.getNimi());
		 
		 File file = new File("Tiedostot" + "\\" + valittuTiedosto.getNimi());
		 System.out.println(file.getAbsolutePath());

			 try {
				Desktop.getDesktop().open(new File(file.getAbsolutePath()));
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		
		 System.out.println(file.getAbsolutePath());
		 }
		


}
