package mallit;

public class Levitystapa {

	private int id;
	private String nimi;
	private double perusmaksu;
	private double aika;
	private double tuntihinta;
	
	public Levitystapa() {
	}
	
	public Levitystapa(int id, String nimi, double perusmaksu, double aika, double tuntihinta) {
		this.id = id;
		this.nimi = nimi;
		this.perusmaksu = perusmaksu;
		this.aika = aika;
		this.tuntihinta = tuntihinta;
		
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getPerusmaksu() {
		return perusmaksu;
	}

	public void setPerusmaksu(double perusmaksu) {
		this.perusmaksu = perusmaksu;
	}

	public double getAika() {
		return aika;
	}

	public void setAika(double aika) {
		this.aika = aika;
	}
	
	public double getTuntihinta() {
		return tuntihinta;
	}

	public void setTuntihinta(double tuntihinta) {
		this.tuntihinta = tuntihinta;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Perusmaksu: " + this.perusmaksu + ", Aika: " + this.aika + ", Tuntihinta: " + this.tuntihinta;
	}
	
}
