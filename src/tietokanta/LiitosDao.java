package tietokanta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mallit.Liitos;

/**
 * 
 * Tama classi sisaltaa CRUD-toiminnallisuudet liitosten lisaamiselle tietokantaan.
 * Classi implementoi Dao-interfacen, jokainen sen metodi toteutetaan.
 *
 */
public class LiitosDao implements Dao<Liitos> {

	/**
	 * Palauttaa haetun alkion parametrina annettavan
	 * id:n perusteella.
	 * 
	 * @param id haettavan liitoksen id
	 */
	@Override
	public Liitos hae(int id) throws SQLException {
		
		// Yhdistetaan tietokantaan DBApurit-luokan metodilla
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu liitos haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Liitokset WHERE id = ?";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			if (rs == null) {
				System.out.println("Liitosta ei loydy");
				// TODO, parempi virheilmoitus
			} 
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		try {

			if (rs.next() == true) {
				return liitosOlioResultSetista(rs);
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
		
		// Palautetaan null jos ei haulla loytynyt mitaan ylempana
		return null;
	}
	/**
	 * Palauttaa tietokannasta kaikki liitokset-taulun alkiot 
	 * 
	 * @return	lista kaikista liitokset-taulun alkioista
	 */
	@Override
	public List<Liitos> haeKaikki() throws SQLException {
		// Yhdistetaan tietokantaan
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// SQL-kysely, jolla haluttu liitos haetaan annetulla parametrilla "id"
		String sql = "SELECT * FROM Liitokset";
		
		// Tulosjoukko, johon haetut tiedot tallennetaan
		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		
		try {
			// SQL-kyselyn muodostus ja suoritus
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
						
			// Lista, johon liitos-oliot tallennetaan
			List<Liitos> liitokset = new ArrayList<Liitos>();
	
			// Iteroidaan rs ResultSetia, muodostetaan jokaisesta sen alkiosta
			// uusi liitos-olio apumetodin liitosOlioResultSetista avulla (implementoitu
			// alempana) ja lisataan jokainen olio listaan
			while (rs.next()) {
				Liitos liitosOlio = liitosOlioResultSetista(rs);
				liitokset.add(liitosOlio);
			}
			
			// Palautetaan lista
			return liitokset;
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
		

		return null;
	}

	/**
	 * Lisaa tietokantaan parametrina annetun Liitos-luokkaolion
	 * attribuutit. Palauttaa true/false riippuen onnistuiko lisays vai ei.
	 * 
	 * @param p	liitos-olio, jonka tiedot halutaan lisata tietokantaan
	 * return	tieto lisayksen onnistumisesta
	 * 
	 */
	@Override
	public boolean lisaa(Liitos p) throws SQLException {
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		// PreparedStatement-olio, jonka avulla SQL-kysely valmistellaan lahetettavaksi
		PreparedStatement ps = null;
		
		// SQL-kysely
		String sql = "INSERT INTO liitokset "
				+ "(nimi, kappalehinta, tuntihinta, asetteen_tekeminen) VALUES (?, ?, ?, ?)";

		try {
			// Valmistellaan kysely ja sijoitetaan siihen halutut arvot taman metodin parametrina saadusta oliosta
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getTuntihinta());
			ps.setDouble(4, p.getAsetteen_tekeminen());
			
			// Suoritetaan kysely
			int i = ps.executeUpdate();
			
			// executeUpdate palauttaa muokattujen eli lisattyjen rivien maaran, 
			// tassa siis pitaa palautusarvona olla 1 jos kaikki meni oikein
			if (i == 1) {
				System.out.println("toimii");
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean paivita(Liitos p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		PreparedStatement ps = null;
		
		String sql = "UPDATE liitokset SET "
				+ "nimi=?, kappalehinta=?, tuntihinta=?, asetteen_tekeminen=?"
				+ " WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNimi());
			ps.setDouble(2, p.getKappalehinta());
			ps.setDouble(3, p.getTuntihinta());
			ps.setDouble(4, p.getAsetteen_tekeminen());
			ps.setInt(5, p.getId());
			
			int i = ps.executeUpdate();

			if (i == 1) {
				return true;
			}	
		} catch (SQLException se){
			se.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean poista(int p) throws SQLException {
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
		PreparedStatement ps = null;
		String sql = "DELETE FROM liitokset WHERE id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, p);
			int i = ps.executeUpdate();
			
			if (i == 1) {
				return true;
			}
		} catch(SQLException se){
			se.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * Apumetodi, jonka avulla parametrina saadusta ResultSetista muodostetaan
	 * Liitos-luokan olio, joka palautetaan.
	 * 
	 * @param rs	ResultSet, jonka tiedot halutaan koota Liitos-luokan olioksi
	 * @return		Liitos-luokan olio
	 * @throws SQLException
	 */
	public Liitos liitosOlioResultSetista(ResultSet rs) throws SQLException {
		
		// Uusi olio Liitos malliluokasta, johon parametrina saadun ResultSetin arvot sijoitetaan settereilla
		Liitos liitosOlio = new Liitos();
		liitosOlio.setId(rs.getInt("id"));
		liitosOlio.setNimi(rs.getString("nimi"));
		liitosOlio.setKappalehinta(rs.getDouble("kappalehinta"));
		liitosOlio.setTuntihinta(rs.getDouble("tuntihinta"));
		liitosOlio.setAsetteen_tekeminen(rs.getDouble("asetteen_tekeminen"));
		
		return liitosOlio;	
	}
	
}
