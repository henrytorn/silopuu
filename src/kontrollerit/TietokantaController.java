package kontrollerit;

import java.io.IOException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tietokanta.PuutavaraDao;

public class TietokantaController {

	
	@FXML
	void avaaPuutavaratIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaPuutavara.fxml"));
    		Stage stage = new Stage();		
    		stage.setTitle("Puutavarat");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaLisamaksutIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaLisamaksu.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisämaksut");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaLiitosIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaLiitos.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Liitokset");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaPintakasittelyaineetIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaPintakasittelyaine.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Pintakäsittelyaineet");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaTarviketIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaTarvike.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Tarvikkeet");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	
	@FXML
	void avaaMuokkaaPintakasittelyaineIkkuna(ActionEvent event) throws SQLException {
		
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/MuokkaaPintakasittelyaine.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Muokkaa pintakasittelyaine");
    		stage.setScene(new Scene(root, 365, 407));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaLevitystapaIkkuna(ActionEvent event) throws SQLException {
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaLevitystapa.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisää levitystapa");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void avaaValmiskappaleIkkuna(ActionEvent event) throws SQLException {
    	Parent root;
    	try {
    		root = FXMLLoader.load(getClass().getClassLoader().getResource("application/TietokantaValmiskappale.fxml"));
    		Stage stage = new Stage();
    		stage.setTitle("Lisää uusi kappale");
    		stage.setScene(new Scene(root, 1024, 720));
    		stage.show();
    	}
    	catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	@FXML
	void poistaPuutavara(ActionEvent event) throws SQLException{

		PuutavaraDao p = new PuutavaraDao();
		if(p.poista(910)) {
			System.out.println("Poisto onnistui");
		}
		else {
			System.out.println("Poisto epäonnistui");
		}
	}



	
}
