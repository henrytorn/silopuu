package mallit;

public class Valmiskappale {
	
	private int id;
	private String nimi;
	private double kappalehinta;
	private double koko;
	private String vari;
	
	public Valmiskappale() {
	}
	
	public Valmiskappale(int id, String nimi, double kappalehinta, double koko, String vari) {
		this.id = id;
		this.nimi = nimi;
		this.kappalehinta = kappalehinta;
		this.koko = koko;
		this.vari = vari;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getKappalehinta() {
		return kappalehinta;
	}

	public void setKappalehinta(double kappalehinta) {
		this.kappalehinta = kappalehinta;
	}

	public String getVari() {
		return vari;
	}

	public void setVari(String vari) {
		this.vari = vari;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Kappalehinta: " + this.kappalehinta + ", Koko: " + this.koko + ", " + ", Vari: "  + this.vari;
	}

	public double getKoko() {
		return koko;
	}

	public void setKoko(double koko) {
		this.koko = koko;
	}
}
