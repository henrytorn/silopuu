package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.ArkistoKappale;
import mallit.Lisapalvelu;
import mallit.Valmiskappale;
import tietokanta.ArkistoKappaleDao;
import tietokanta.DBApurit;
import tietokanta.ValmiskappaleDao;

public class LuoTuoteValmiskappaleViewController implements Initializable{

	@FXML 
	private TableView<ArkistoKappale> table;
	
	@FXML 
	private TableColumn<ArkistoKappale, Integer> id;
	
	@FXML 
	private TableColumn<ArkistoKappale, String> nimi;
	
	@FXML 
	private TableColumn<ArkistoKappale, Double> kappalehinta;
	
	@FXML 
	private TableColumn<ArkistoKappale, String> koko;
	
	@FXML 
	private TableColumn<ArkistoKappale, String> vari;

	@FXML
	public Button lisaaBtn;
	
	@FXML
	public Button peruutaBtn;
	
	@FXML
	private Spinner kappalemaaraSpinner;
	
	private ArkistoKappale valittuKappale;
	
	private ArkistoKappaleDao dao = new ArkistoKappaleDao();
	
	private ObservableList<ArkistoKappale> arkistoKappaleObsLista = FXCollections.observableArrayList();
	private List<ArkistoKappale> arkistoKappaleLista = new ArrayList<ArkistoKappale>();
	private LuoTuoteController tama;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			arkistoKappaleLista = dao.haeKaikki();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (ArkistoKappale x : arkistoKappaleLista) {
			arkistoKappaleObsLista.add(x);
		}
				
		id.setCellValueFactory(new PropertyValueFactory<ArkistoKappale, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<ArkistoKappale, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<ArkistoKappale, Double>("hinta"));
		koko.setCellValueFactory(new PropertyValueFactory<ArkistoKappale, String>("koko"));
		vari.setCellValueFactory(new PropertyValueFactory<ArkistoKappale, String>("vari"));
		
		table.setItems(arkistoKappaleObsLista);
		

	}
	
	@FXML 
	void Lisaa(ActionEvent event) throws SQLException {

			//?
		
  		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
  		
		int maara = (Integer) kappalemaaraSpinner.getValue();

		try {
			
			loader.load();
			LuoTuoteController controller = loader.getController();
	    	ArkistoKappale valittuValmiskappale = table.getSelectionModel().getSelectedItem();
	    	setValittuKappale(valittuValmiskappale);

	    	controller.setValittuKappale(valittuValmiskappale);
			controller.setValittuLiitos(tama.getValittuLiitos());
			controller.setValittuTarvike(tama.getValittuTarvike());
			
	    	if (valittuValmiskappale != null) {
	    		for(int i = 0; i < maara  ; i++)
	    		{
				controller.getListItems().add(valittuValmiskappale.getNimi());
				controller.getListItems().add("Koko: " + valittuValmiskappale.getKoko());
				controller.getListItems().add("Väri: " + valittuValmiskappale.getVari());
				controller.getListItems().add("Kappalehinta: " + Double.toString(valittuValmiskappale.getHinta()));		
				controller.getListItems().add("");
	    		}
		}}
	    	catch (IOException e) {
			
			e.printStackTrace();
		}

			Parent root = loader.getRoot();
	    	Stage stage = new Stage();
			stage.setTitle("Luo tuote");
			stage.setScene(new Scene(root, 1280, 768));
			stage.show();
			
			Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
			currStage.hide();
		
	}
	
	@FXML 
	void Peruuta(ActionEvent event) throws SQLException {

		PaaikkunaController controller = new PaaikkunaController();
		controller.avaaTuoteLisays(event);
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}

	public ArkistoKappale getValittuKappale() {
		return valittuKappale;
	}

	public void setValittuKappale(ArkistoKappale valittuKappale) {
		this.valittuKappale = valittuKappale;
	}

	public LuoTuoteController getTama() {
		return tama;
	}

	public void setTama(LuoTuoteController tama) {
		this.tama = tama;
	}
	
	
}