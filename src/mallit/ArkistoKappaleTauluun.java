package mallit;

public class ArkistoKappaleTauluun {
	
	private int id, pintakasittely_id, puutavara_id, liitos_id, tarvike_id, levitystapa_id, lisamaksu_id;
	private String nimi, pintakasittely, puutavara, levitystapa, lisamaksu;
	private double leveys, korkeus, syvyys, hinta;
	
	public ArkistoKappaleTauluun() {
	}
	
	public ArkistoKappaleTauluun(int id, String nimi, double leveys, double korkeus, double syvyys, double hinta, String pintakasittely, 
			String puutavara, String levitystapa, String lisamaksu, int pintakasittely_id, int puutavara_id, 
			int liitos_id, int tarvike_id, int levitystapa_id, int lisamaksu_id) {
		
		this.id = id;
		this.nimi = nimi;
		this.leveys = leveys;
		this.korkeus = korkeus;
		this.syvyys = syvyys;
		this.hinta = hinta;
		this.pintakasittely = pintakasittely;
		this.puutavara = puutavara;
		this.levitystapa = levitystapa;
		this.lisamaksu = lisamaksu;
		this.pintakasittely_id = pintakasittely_id;
		this.puutavara_id = puutavara_id;
		this.levitystapa_id = levitystapa_id;
		this.lisamaksu_id = lisamaksu_id;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public String getPintakasittely() {
		return pintakasittely;
	}

	public void setPintakasittely(String pintakasittely) {
		this.pintakasittely = pintakasittely;
	}

	public String getPuutavara() {
		return puutavara;
	}

	public void setPuutavara(String puutavara) {
		this.puutavara = puutavara;
	}

	public String getLevitystapa() {
		return levitystapa;
	}

	public void setLevitystapa(String levitystapa) {
		this.levitystapa = levitystapa;
	}

	public String getLisamaksu() {
		return lisamaksu;
	}

	public void setLisamaksu(String lisamaksu) {
		this.lisamaksu = lisamaksu;
	}

	public double getLeveys() {
		return leveys;
	}

	public void setLeveys(double leveys) {
		this.leveys = leveys;
	}

	public double getKorkeus() {
		return korkeus;
	}

	public void setKorkeus(double korkeus) {
		this.korkeus = korkeus;
	}

	public double getSyvyys() {
		return syvyys;
	}

	public void setSyvyys(double syvyys) {
		this.syvyys = syvyys;
	}

	public double getHinta() {
		return hinta;
	}

	public void setHinta(double hinta) {
		this.hinta = hinta;
	}

	
	public int getPintakasittely_id() {
		return pintakasittely_id;
	}

	public void setPintakasittely_id(int pintakasittely_id) {
		this.pintakasittely_id = pintakasittely_id;
	}

	public int getPuutavara_id() {
		return puutavara_id;
	}

	public void setPuutavara_id(int puutavara_id) {
		this.puutavara_id = puutavara_id;
	}

	public int getLiitos_id() {
		return liitos_id;
	}

	public void setLiitos_id(int liitos_id) {
		this.liitos_id = liitos_id;
	}

	public int getTarvike_id() {
		return tarvike_id;
	}

	public void setTarvike_id(int tarvike_id) {
		this.tarvike_id = tarvike_id;
	}

	public int getLevitystapa_id() {
		return levitystapa_id;
	}

	public void setLevitystapa_id(int levitystapa_id) {
		this.levitystapa_id = levitystapa_id;
	}

	public int getLisamaksu_id() {
		return lisamaksu_id;
	}

	public void setLisamaksu_id(int lisamaksu_id) {
		this.lisamaksu_id = lisamaksu_id;
	}

	@Override
	public String toString() {
		return this.nimi;
		//TODO
	}


}
