package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.Liitos;
import mallit.Puutavara;
import mallit.Tarvike;
import tietokanta.DBApurit;
import kontrollerit.Apumetodeja;

public class LisaaTuoteTarvikeController implements Initializable{
	
	@FXML 
	private TableView<Tarvike> table;
	
	@FXML 
	private TableColumn<Tarvike, Integer> id;
	
	@FXML 
	private TableColumn<Tarvike, String> nimi;
	
	@FXML 
	private TableColumn<Tarvike, Double> kappalehinta;
	
	@FXML 
	private TableColumn<Tarvike, Double> toimenpidehinta;
	
	@FXML 
	private TableColumn<Tarvike, Double> toimenpideaika;

	@FXML 
	private Button tallennaTarvikeBtn;
	
	@FXML 
	private Button peruutaTarvikeBtn;
	
	@FXML 
	private Spinner tarvikemaaraSpinner;
	
	@FXML 
	private TextField kokonaisHinta;
	
	@FXML
	private Integer maara = null;
	
	@FXML
	private Double yhteishinta = null;
	
	private ObservableList<Tarvike> Tarvikelist = FXCollections.observableArrayList();
	private LuoTuoteController tama;

	@FXML
	void tallennaTarvike(ActionEvent event) throws SQLException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("application/LuoTuote.fxml"));
		

		
		try {
			
			loader.load();
			LuoTuoteController controller = loader.getController();
			Tarvike valittuTarvike = table.getSelectionModel().getSelectedItem();
			
	    	if (valittuTarvike != null) {
	    		
	    		controller.setValittuTarvike(valittuTarvike);
	    		controller.setValittuKappale(tama.getValittuKappale());
	    		controller.setValittuLiitos(tama.getValittuLiitos());
	    		
				controller.getListItems().add(valittuTarvike.getNimi());
				controller.getListItems().add("- kappalehinta: " + Double.toString(valittuTarvike.getKappalehinta()));
				controller.getListItems().add("- Määrä: " + Integer.toString((int) tarvikemaaraSpinner.getValue()));
				controller.getListItems().add("- Toimenpidehinta: " + valittuTarvike.getToimenpidehinta());
				controller.getListItems().add("- Toimenpideaika: " + Double.toString(valittuTarvike.getToimenpideaika()));	
				controller.getListItems().add("- Kokonaishinta: " + laskeHinta());
				controller.getListItems().add("");
	    		
		}}
	    	catch (IOException e) {
			
			e.printStackTrace();
		}
				
			
		
		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Luo tuote");
		stage.setScene(new Scene(root, 1280, 768));
		stage.show();
		
		Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		currStage.hide();
	}
	

	
	@FXML
	void peruutaTarvike(ActionEvent event) throws SQLException {

	        		Stage stages = (Stage) tallennaTarvikeBtn.getScene().getWindow();
	        		stages.close();
	        		System.out.print("peruutettu");
	            	
	            }
	        	    
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();

		String sql = "SELECT * FROM Helat";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
				
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
		
			while (rs.next()) {
				Tarvikelist.add(new Tarvike(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("kappalehinta"),rs.getDouble("toimenpidehinta"), rs.getDouble("toimenpideaika")));
				
			}
			
			} catch (SQLException se) {
				se.printStackTrace();
			}
	
		id.setCellValueFactory(new PropertyValueFactory<Tarvike, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<Tarvike, String>("nimi"));
		kappalehinta.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("kappalehinta"));
		toimenpidehinta.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("toimenpidehinta"));
		toimenpideaika.setCellValueFactory(new PropertyValueFactory<Tarvike, Double>("toimenpideaika"));
		table.setItems(Tarvikelist);
		
		//tablen listener
		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    if (newSelection != null) {
	    	laskeHinta();
	    }    
	    
		});
		
		tarvikemaaraSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue != oldValue) {
				laskeHinta();
			}
		});

	}
		
		private String laskeHinta() {

		Tarvike valittuTarvike = table.getSelectionModel().getSelectedItem();
		
		
		int maara = (int) tarvikemaaraSpinner.getValue();
		double kappalehinta = valittuTarvike.getKappalehinta();
		double toimenpidehinta = valittuTarvike.getToimenpidehinta();
		double toimenpideaika = valittuTarvike.getToimenpideaika();
		double yhteishinta = (kappalehinta * maara + (toimenpidehinta / 60 * toimenpideaika));
		
		
		String tulos = String.valueOf(yhteishinta);
		kokonaisHinta.setText(tulos);
		
		return tulos;

		}



		public LuoTuoteController getTama() {
			return tama;
		}



		public void setTama(LuoTuoteController tama) {
			this.tama = tama;
		}
		
}
	

	


