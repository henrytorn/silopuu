package mallit;

public class Liitos {
	
	private int id;
	private String nimi;
	private double kappalehinta;
	private double tuntihinta;
	private double asetteen_tekeminen;
	
	public Liitos() {
	}
	
	public Liitos(int id, String nimi, double kappalehinta, double tuntihinta, double asetteen_tekeminen) {
		this.id = id;
		this.nimi = nimi;
		this.kappalehinta = kappalehinta;
		this.tuntihinta = tuntihinta;
		this.asetteen_tekeminen = asetteen_tekeminen;
		
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getKappalehinta() {
		return kappalehinta;
	}

	public void setKappalehinta(double kappalehinta) {
		this.kappalehinta = kappalehinta;
	}

	public double getTuntihinta() {
		return tuntihinta;
	}

	public void setTuntihinta(double tuntihinta) {
		this.tuntihinta = tuntihinta;
	}
	
	public double getAsetteen_tekeminen() {
		return asetteen_tekeminen;
	}

	public void setAsetteen_tekeminen(double asetteen_tekeminen) {
		this.asetteen_tekeminen = asetteen_tekeminen;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", Nimi: " + this.nimi + ", Hinta: " + this.kappalehinta + ", Tuntihinta: " + this.tuntihinta + ", asetteen tekeminen (min): " + this.asetteen_tekeminen;
	}
}
