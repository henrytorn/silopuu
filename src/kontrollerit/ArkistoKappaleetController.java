package kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mallit.ArkistoKappale;
import mallit.ArkistoKappaleTauluun;
import mallit.Lisapalvelu;
import tietokanta.ArkistoKappaleDao;
import tietokanta.ArkistoKappaleTauluunDao;
import tietokanta.DBApurit;

public class ArkistoKappaleetController implements Initializable {
	private ArkistoKappaleDao dao = new ArkistoKappaleDao();
	private ArkistoKappaleTauluunDao dao1 = new ArkistoKappaleTauluunDao();
	
	@FXML 
	private TableView<ArkistoKappaleTauluun> kappaleetTable;
	@FXML
	private TableColumn<ArkistoKappaleTauluun, Integer> id;
	
	@FXML
	private TableColumn<ArkistoKappaleTauluun, String> nimi, pintakasittely, puutavara, levitystapa, lisapalvelu;
	@FXML
	private TableColumn<ArkistoKappaleTauluun, Double> leveys, korkeus, syvyys, hinta;
	@FXML
	private TextField hakuTBox;
	
	private ObservableList<ArkistoKappaleTauluun> kappaleObsList1 = FXCollections.observableArrayList();
	private List<ArkistoKappaleTauluun> kappaleList1;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		try {
			kappaleList1 = dao1.haeKaikki();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (ArkistoKappaleTauluun x : kappaleList1) {
			kappaleObsList1.add(x);
		}
		
		id.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, Integer>("id"));
		nimi.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, String>("nimi"));
		leveys.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, Double>("leveys"));
		korkeus.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, Double>("korkeus"));
		syvyys.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, Double>("syvyys"));
		hinta.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, Double>("hinta"));
		pintakasittely.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, String>("pintakasittely"));
		puutavara.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, String>("puutavara"));
		levitystapa.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, String>("levitystapa"));
		lisapalvelu.setCellValueFactory(new PropertyValueFactory<ArkistoKappaleTauluun, String>("lisamaksu"));
		
		kappaleetTable.setItems(kappaleObsList1);
		
		DBApurit apu = new DBApurit();
		Connection conn = apu.yhdistaDb();
			
		// TODO katso haeKappaleet metodin kommentti
//		hakuTBox.textProperty().addListener((obs, oldText, newText) -> {
//			try {
//				haeKappaleet(conn, newText);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		});
	}
	
	@FXML
	void avaaLuoKappaleKopio(ActionEvent event) throws IOException, SQLException {
		FXMLLoader loader = new FXMLLoader();
  		loader.setLocation(getClass().getClassLoader().getResource("application/LuoKappaleKopio.fxml"));
		loader.load();

		LuoKappaleKopioController controller = loader.getController();

		Parent root = loader.getRoot();
		Stage stage = new Stage();
		stage.setTitle("Kappaleen kopiointi");
		stage.setScene(new Scene(root, 450, 600));
		stage.show();
		
		//Stage currStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		//currStage.close();
	}
	
	
	@FXML
	void poistaKappale(ActionEvent event) throws SQLException {
		
		//Taulusta kappale
		ArkistoKappaleTauluun valittuKappale = kappaleetTable.getSelectionModel().getSelectedItem();
		
		// Poistetaan ArkistoKappaleDao olion avulla arkisto_kappaleet taulusta, EI ArkistoKappaleTauluunDao:n (dao = new ArkistoKappaleDao)
		dao.poista(valittuKappale.getId());
		
		kappaleObsList1.removeAll(kappaleObsList1);
		initialize(null, null);
	}
	
	
	// TODO Ei toimi oikein, vaatii aika paljon saatoa, tehdaan myohemmin
	
//	@FXML
//	void haeKappaleet(Connection conn, String text) throws SQLException {
//		String sql = "SELECT * FROM arkisto_kappaleet WHERE nimi LIKE '%" + text + "%'";
//
//		ResultSet rs = null;
//		PreparedStatement ps = null;
//
//		try {
//			ps = conn.prepareStatement(sql);
//			rs = ps.executeQuery();
//
//			kappaleObsList1.clear();
//
//			while (rs.next()) {
//				kappaleObsList1.add(new ArkistoKappaleTauluun(rs.getInt("id"),rs.getString("nimi"),rs.getDouble("leveys"), 
//						rs.getDouble("korkeus"), rs.getDouble("syvyys"), rs.getDouble("hinta"), rs.getString("pintakasittely"), rs.getString("puutavara"), 
//						rs.getString("liitos"), rs.getString("tarvike"), rs.getString("lisamaksu"),
//						rs.getString("levitystapa"), rs .getInt("pintakasittely_id"), rs.getInt("puutavara_id"), rs.getInt("liitos_id"), 
//						rs.getInt("tarvike_id"), rs.getInt("levitystapa_id"), rs.getInt("lisamaksu_id")));
//			}
//
//		} catch (SQLException se) {
//			se.printStackTrace();
//		}
//
//	}
}
